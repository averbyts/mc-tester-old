/*
   HepMCParticle class implementation

   AUTHOR:      Nadia Davidson
   LAST UPDATE: 2008-02-07
*/
#include "HepMCParticle.H"
#include "HepMCEvent.H"
#include <iostream>

using namespace std;

#ifdef _USE_ROOT_
ClassImp(HepMCParticle)
#endif

HepMCParticle::HepMCParticle(){}

HepMCParticle::HepMCParticle(HepMC::GenParticle & particle, HEPEvent * e, int Id){

  part = &particle;
  SetEvent(e);
  SetId(Id);
}

HepMCParticle::~HepMCParticle(){
}

const HepMCParticle HepMCParticle::operator=(HEPParticle &p)   
{
  if (this == &p)
    return *this;

  // SetId(p.GetId());

  SetPDGId(p.GetPDGId());
  SetStatus(p.GetStatus());
  SetMother(p.GetMother());
  SetMother2(p.GetMother2());
  SetFirstDaughter(p.GetFirstDaughter());
  SetLastDaughter(p.GetLastDaughter());
  SetE(p.GetE());
  SetPx(p.GetPx());
  SetPy(p.GetPy());
  SetPz(p.GetPz());
  SetM(p.GetM());
  SetVx(p.GetVx());
  SetVy(p.GetVy());
  SetVz(p.GetVz());
  SetTau(p.GetTau());

  return *this;

}

HEPEvent* HepMCParticle::GetEvent(){ 
  return event; 
}

int const HepMCParticle::GetId(){ 
  return id; 
}

//GetMother and Daughter methods not implemented here
//GetDaughterList() and GetMotherList() should be used
//instead. Still to do: some errors should be thrown.
int const HepMCParticle::GetMother(){ 
  return 0; 
}

int const HepMCParticle::GetMother2(){ 
  return 0; 
}

int const HepMCParticle::GetFirstDaughter(){
  return 0; 
}

int const HepMCParticle::GetLastDaughter(){ 
  return 0; 
}

double const HepMCParticle::GetE(){ 
  return part->momentum().e(); 
}

double const HepMCParticle::GetPx(){
  return part->momentum().px();  
}

double const HepMCParticle::GetPy(){
  return part->momentum().py(); 
}

double const HepMCParticle::GetPz(){
  return part->momentum().pz(); 
} 

double const HepMCParticle::GetM(){
  return part->momentum().m(); 
}

int const HepMCParticle::GetPDGId(){ 
  return part->pdg_id(); 
}

int const HepMCParticle::GetStatus(){ 
  return part->status(); 
}

int const HepMCParticle::IsStable(){ 
  return (GetStatus() == 1 || !part->end_vertex());
}

int const HepMCParticle::Decays(){
  return (!IsHistoryEntry() && !IsStable());
}

int const HepMCParticle::IsHistoryEntry(){
  return (GetStatus() == 3);
}

double const HepMCParticle::GetVx(){
  if(part->production_vertex()) return part->production_vertex()->position().x();
  return 0.;
}

double const HepMCParticle::GetVy(){
  if(part->production_vertex()) return part->production_vertex()->position().y();
  return 0.;
}

double const HepMCParticle::GetVz(){
  if(part->production_vertex()) return part->production_vertex()->position().z();
  return 0.;
}

double const HepMCParticle::GetTau(){ 
  //Not implemented
  if(part->end_vertex()&&part->production_vertex())
    return (part->end_vertex()->position().t()-part->production_vertex()->position().t()); //not correct, but will see if it's empty
  else
    return 0;
}

//methods not implemented. Not done for HepMC.
/**void HepMCParticle::SetP4(MC4Vector &v){ }
void HepMCParticle::SetP3(MC3Vector &v){ }
void HepMCParticle::SetV3(MC3Vector &v){ }
**/

void HepMCParticle::SetEvent(HEPEvent * event){
  this->event=(HepMCEvent*)event;
}

void HepMCParticle::SetId( int id ){
  this->id = id;
}

//Can not use these methods for HepMC
void HepMCParticle::SetMother( int mother ){}
void HepMCParticle::SetMother2( int mother ){}
void HepMCParticle::SetFirstDaughter( int daughter ){}
void HepMCParticle::SetLastDaughter ( int daughter ){}

void HepMCParticle::SetE( double E ){
  HepMC::FourVector temp_mom(part->momentum());
  temp_mom.setE(E);
  part->set_momentum(temp_mom); 
}

void HepMCParticle::SetPx( double px ){
  HepMC::FourVector temp_mom(part->momentum());
  temp_mom.setPx(px);
  part->set_momentum(temp_mom); 
}

void HepMCParticle::SetPy( double py ){
  HepMC::FourVector temp_mom(part->momentum());
  temp_mom.setPy(py);
  part->set_momentum(temp_mom); 
}

void HepMCParticle::SetPz( double pz ){
  HepMC::FourVector temp_mom(part->momentum());
  temp_mom.setPz(pz);
  part->set_momentum(temp_mom); 
} 

void HepMCParticle::SetM( double m ){
  //Can not set in HepMC::GenEvent
  cout << "Can not set mass in HepMCParticle. Set e, px, py, pz instead" <<endl;
}

void HepMCParticle::SetPDGId ( int pdg ){
  part->set_pdg_id( pdg );
}

void HepMCParticle::SetStatus( int st){
  part->set_status( st );
}

void HepMCParticle::SetVx ( double vx){
  if(part->production_vertex()) part->production_vertex()->point3d().setX(vx);
}

void HepMCParticle::SetVy ( double vy){
  if(part->production_vertex()) part->production_vertex()->point3d().setY(vy);
}

void HepMCParticle::SetVz ( double vz){
  if(part->production_vertex()) part->production_vertex()->point3d().setZ(vz);
}

void HepMCParticle::SetTau( double tau){
  //Not implemented
}


HEPParticleList* HepMCParticle::GetDaughterList(HEPParticleList *list)
{
  // if list is not provided, it is created.
  if (!list) list=new HEPParticleList();

  if(!part->end_vertex()) //no daughters
    return list;

  HepMC::GenVertex::particles_out_const_iterator pcle_itr;
  pcle_itr = part->end_vertex()->particles_out_const_begin(); 

  HepMC::GenVertex::particles_out_const_iterator pcle_itr_end;
  pcle_itr_end = part->end_vertex()->particles_out_const_end();

  //iterate over daughters
  for(; pcle_itr != pcle_itr_end; pcle_itr++ ){
    HepMCParticle * daughter = event->GetParticleWithBarcode((*pcle_itr)->barcode());
    if(!list->contains(daughter->GetId())){
      if(!daughter->IsHistoryEntry())
	list->push_back(daughter);
    }
  }
  return list;  
}

HEPParticleList* HepMCParticle::GetMotherList(HEPParticleList *list)
{
   // if list is not provided, it is created.
   if (!list) list=new HEPParticleList();

   if(!part->production_vertex()) //no mothers
     return list;

   HepMC::GenVertex::particles_in_const_iterator pcle_itr; 
   pcle_itr = part->production_vertex()->particles_in_const_begin(); 
   HepMC::GenVertex::particles_in_const_iterator pcle_itr_end;
   pcle_itr_end = part->production_vertex()->particles_in_const_end();

   for(; pcle_itr != pcle_itr_end; pcle_itr++){
      list->push_back(event->GetParticleWithBarcode((*pcle_itr)->barcode()));
   }

   return list;
}


#ifdef _USE_ROOT_
void HepMCParticle::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}

#endif

