/*
   THEPParticle class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 1999-04-21
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "THEPParticle.H"
#include "THEPEvent.H"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef _USE_ROOT_
ClassImp(THEPParticle)
#endif



THEPParticle *nullHEPParticle=new THEPParticle();




THEPParticle::THEPParticle(int id, int pdgid,int status, 
			   int mother1,   int mother2, 
			   int daughter1, int daughter2,
			   double px, double py, double pz, double E, double M,
			   double vx, double vy, double vz, double tau):
  fEvent(0),
  fId(id),
  fPDG(pdgid),
  fStatus(status),
  fMother1(mother1),
  fMother2(mother2),
  fDaughter1(daughter1),
  fDaughter2(daughter2),
  fPx(px),
  fPy(py),
  fPz(pz),
  fE(E),
  fM(M),
  fVx(vx),
  fVy(vy),
  fVz(vz),
  fVtau(tau)
{
  // constructs a THEPParticle object according to provided data.
}

const THEPParticle THEPParticle::operator=(const THEPParticle &p)
{
  // assignment operator - assigns another THEPParticle object to
  // this one i.e.
  //
  // THEPParticle pion;
  // THEPParticle copy_of_pion;
  //
  // copy_of_pion = pion;

  fEvent = 0;
  fId  = p.fId;
  fPDG = p.fPDG;
  fStatus=p.fStatus;

  fMother1=p.fMother1;
  fMother2=p.fMother2;
  
  fDaughter1=p.fDaughter1;
  fDaughter2=p.fDaughter2;
 
  fPx=p.fPx;
  fPy=p.fPy;
  fPz=p.fPz;
  fE=p.fE;
  fM=p.fM;

  fVx=p.fVx;
  fVy=p.fVy;
  fVz=p.fVz;
  fVtau=p.fVtau;
  return *this;
}



bool THEPParticle::isEqual( THEPParticle &p)
{
  // determines equality of objects - returns 1 if compared object
  // is exactly the same as this - otherwise returns 0
  // This function is used by equality and inequality operators.
  //
  // object that is compared to this one must have exactly
  // the same momenta,vertex, PDGCode, status, and all
  // identifiers: mothers', daughters' and itself.
  //
  // i.e.: comparison of two THEPEvent objects is (among others)
  // comparison of all particles - they must be at the same positions
  // and have all its data the same:
  //
  //   for (int i=1; i<=fNumOfParticles; i++) {
  //     THEPParticle p  =  *( fParticles[i]);
  //     THEPParticle ep = *(e.fParticles[i]);
  //
  //     if ( p != ep ) {
  //       printf("Difference in particle %i\n",i);
  //       return  0;
  //     }
  //   }
  //
  // See also:  
  //           THEPParticle::operator==  
  //           THEPParticle::operator!=  
  //           THEPParticle::Compare_WithoutId

  
  bool result =  ( (fId        == p.fId       ) &&
		   (fMother1   == p.fMother1  ) &&
		   (fMother2   == p.fMother2  ) &&
		   (fDaughter1 == p.fDaughter1) &&
		   (fDaughter2 == p.fDaughter2) &&		   
		   Compare_WithoutId(p));

  return result;
}


void THEPParticle::Diff(HEPParticle &pp)
{
  // Shows a differences between this particle and another.
  // For floats absolute and relative difference is calculated
  // (the latter given in percents). If compared values is equal
  // to 0.0 relative difference (x2-x1)/x1 cannot be calculated.
  // In this case displayed relative difference will be -100%
  //
  // example:
  //
  // TPhotosParticle p1 = MyEvent1.GetParticle(1);
  // TPhotosParticle p2 = MyEvent2.GetParticle(1);
  // /* print differences*/
  // p1.Diff(p2);

  THEPParticle &p = (*( (THEPParticle*)(&pp)));
  if ( (*this) != p) {
    printf("######################################################################\n");
    printf("Particles' differences:\n");
    char full[] = "full";
    ls(full);
    p.ls(full);
    printf("------------------------------------------------------------\n");
    if (fId != p.fId )
      printf("Id:             %15i | %15i\n",fId,p.fId);
    if (fMother1 != p.fMother1  )
      printf("Mother1:        %15i | %15i\n",fMother1,p.fMother1);
    if (fMother2 != p.fMother2  )
      printf("Mother2:        %15i | %15i\n",fMother2,p.fMother2);
    if (fDaughter1 != p.fDaughter1)
      printf("First Daughter: %15i | %15i\n",fDaughter1,p.fDaughter1);
    if (fDaughter2 != p.fDaughter2)
      printf("Last Daughter:  %15i | %15i\n",fDaughter2,p.fDaughter2);

    // the rest is done by inherited one:
    HEPParticle::Diff(p);

    printf("######################################################################\n");
  }

}


void THEPParticle::ls(char *option)
{
  // prints the information about the particle in one of formats selected
  // by (*option) parameter:
  //
  // if option is null (defalut) - prints a short information with
  //                               all values in low precision
  // if option is "P"            - prints momentum components with high
  //                               precision
  // otherwise (option != "P")   - long printout of ALL data, with quite
  //                               low presision of momentum/vertex coords.

  if (option) {
   
    if(strstr(option,"P")) {
    printf("%3i %8s <%3i,%3i;%3i,%3i> (%15.9g,%15.9g,%15.9g) %15.9g %15.9g \n",
	   GetId(), GetParticleName(),  GetMother(), GetMother2(),
	   GetFirstDaughter(), GetLastDaughter(),
	   GetPx(), GetPy(), GetPz(), GetE(), GetM());

    }else{ //full
 
    printf("%3i %8s %5i <%3i,%3i;%3i,%3i>[%4i](%9.3g,%9.3g,%9.3g) %9.3g %9.3g (%7.3g,%7.3g,%7.3g) %5.3e\n",
	   GetId(), GetParticleName(),GetPDGId(),
	   GetMother(), GetMother2(),
	   GetFirstDaughter(), GetLastDaughter(),
	   GetStatus(),
	   GetPx(), GetPy(), GetPz(), GetE(), GetM(),
	   GetVx(), GetVy(), GetVz(), GetTau()              );
    }
  } else {

    printf("%3i %8s <%3i,%3i> [%2i] (%9.3f,%9.3f,%9.3f) %9.3f %9.3f\n",
	   GetId(),
	   GetParticleName(), 
	   GetMother(), GetMother2(),
	   GetStatus(),
	   GetPx(), GetPy(), GetPz(), GetE(), GetM() );
  }
}


void THEPParticle::SetEvent(HEPEvent *event)
{
  fEvent=(THEPEvent*)event;
}

void THEPParticle::SetMother( THEPParticle *mother )
{
  if (mother==0) {
    printf("WARNING!  THEPParticle::SetMother() setting mother to 0 for:\n");
    char full[] = "full";
    ls(full);
    fMother1=0;

  } else {

    if (fEvent) {
 
      int id=fEvent->GetIdOf(mother);
      if (id==0){
	printf("Error in THEPParticle::SetMother(). This particle is out of the event.\n");
	printf("Opertaion cancelled.\n");
      }else{
	fMother1=id;
      }

	
    } else { // if !fEvent
    
	fMother1=mother->GetId();
      
      
    } // if fEvent
    
  } // if mother==0    

}

void THEPParticle::SetMother2( THEPParticle *mother )
{

  if (mother==0) {
    printf("WARNING!  THEPParticle::SetMother2() setting mother to 0 for:\n");
    char full[] = "full";
    ls(full);
    fMother2=0;

  } else {

    if (fEvent) {
 
      int id=fEvent->GetIdOf(mother);
      if (id==0){
	printf("Error in THEPParticle::SetMother2(). This particle is out of the event.\n");
	printf("Opertaion cancelled.\n");
      }else{
	fMother2=id;
      }

	
    } else { // if !fEvent
    
	fMother2=mother->GetId();
      
      
    } // if fEvent
    
  } // if mother==0    

}

void THEPParticle::SetFirstDaughter( THEPParticle *daughter )
{
  if (daughter==0) {
    printf("WARNING!  THEPParticle::SetFirstDaughter() setting daughter to 0 for:\n");
    char full[] = "full";
    ls(full);
    fDaughter1=0;

  } else {

    if (fEvent) {
 
      int id=fEvent->GetIdOf(daughter);
      if (id==0){
	printf("Error in THEPParticle::SetFirstDaughter(). This particle is out of the event.\n");
	printf("Opertaion cancelled.\n");
      }else{
	fDaughter1=id;
      }

	
    } else { // if !fEvent
    
	fDaughter1=daughter->GetId();
      
      
    } // if fEvent
    
  } // if daughter==0    

}

void THEPParticle::SetLastDaughter ( THEPParticle *daughter )
{
  if (daughter==0) {
    printf("WARNING!  THEPParticle::SetLastDaughter() setting daughter to 0 for:\n");
    char full[] = "full";
    ls(full);
    fDaughter2=0;

  } else {

    if (fEvent) {
 
      int id=fEvent->GetIdOf(daughter);
      if (id==0){
	printf("Error in THEPParticle::SetLastDaughter(). This particle is out of the event.\n");
	printf("Opertaion cancelled.\n");
      }else{
	fDaughter2=id;
      }

	
    } else { // if !fEvent
    
	fDaughter2=daughter->GetId();
      
      
    } // if fEvent
    
  } // if daughter==0    

}

THEPEvent* THEPParticle::Event()
{
  return fEvent;
}

THEPParticle* THEPParticle::Mother()
{
  THEPParticle *mother=0;

  if ( fMother1 && fEvent){
    mother = fEvent->GetHEPParticle(fMother1);
  }
  return mother;
}

THEPParticle* THEPParticle::Mother2()
{
  THEPParticle *mother=0;
  if ( fMother2 && fEvent){
    mother = fEvent->GetHEPParticle(fMother2);
  }
  return mother;
}

THEPParticle* THEPParticle::FirstDaughter()
{
  THEPParticle *daughter=0;
  if ( fDaughter1 && fEvent){
    daughter = fEvent->GetHEPParticle(fDaughter1);
  }
  return daughter;

}

THEPParticle* THEPParticle::LastDaughter()
{
  THEPParticle *daughter=0;
  if ( fDaughter2 && fEvent){
    daughter = fEvent->GetHEPParticle(fDaughter2);
  }
  
  return daughter;

}

THEPParticle* THEPParticle::PrevDaughter()
{
  THEPParticle *daughter=0;

  if ( fEvent){
    THEPParticle *first = ((THEPParticle*)Mother())->FirstDaughter();
    THEPParticle *last  = ((THEPParticle*)Mother())->LastDaughter();


    if ( first && last && GetId() > 0  &&
	 ( GetId() >  first->GetId() ) && 
	 ( GetId() <= last->GetId()  ) )
      daughter=fEvent->GetHEPParticle(GetId()-1);

  }
  return daughter;

}

THEPParticle* THEPParticle::NextDaughter()
{
  THEPParticle *daughter=0;

  if ( fEvent){
    THEPParticle *first = (THEPParticle*)Mother()->FirstDaughter();
    THEPParticle *last  = (THEPParticle*)Mother()->LastDaughter();


    if ( first && last && GetId() > 0  &&
	 ( GetId() >= first->GetId() ) && 
	 ( GetId() <  last->GetId()  ) )
      daughter=fEvent->GetHEPParticle(GetId()+1);

  }
  return daughter;

}



void THEPParticle::CastError(char *funcName)
{
  printf("CASTING ERROR: called THEPParticle::%s.\n Exiting.\n",funcName);
  exit(-1);
}


#ifdef _USE_ROOT_
void THEPParticle::Streamer(TBuffer &R__b)
{
  // Stream an object of class THEPEvent.
  
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(); if (R__v) { }
      R__b >> fId;
      R__b >> fPDG;
      R__b >> fStatus;
      
      R__b >> fMother1;
      R__b >> fMother2;
      R__b >> fDaughter1;
      R__b >> fDaughter2;
      
      R__b >> fPx;
      R__b >> fPy;
      R__b >> fPz;
      R__b >> fE; 
      R__b >> fM; 
      
      R__b >> fVx;
      R__b >> fVy;
      R__b >> fVz;
      R__b >> fVtau;
      
      fEvent = 0;

   } else {
      R__b.WriteVersion(THEPParticle::IsA());
      R__b << fId;
      R__b << fPDG;
      R__b << fStatus;
      
      R__b << fMother1;
      R__b << fMother2;
      R__b << fDaughter1;
      R__b << fDaughter2;
      
      R__b << fPx;
      R__b << fPy;
      R__b << fPz;
      R__b << fE; 
      R__b << fM; 
      
      R__b << fVx;
      R__b << fVy;
      R__b << fVz;
      R__b << fVtau; 

   }
}
#endif

