/*
   LUJETSParticle class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 2000-01-24
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "LUJETSParticle.H"
#include "LUJETSEvent.H"

#ifdef _USE_ROOT_
ClassImp(LUJETSParticle)
#endif


LUJETSParticle::LUJETSParticle()
{
 id=1;
 event=&LUJETS;
}

void LUJETSParticle::ls (char* option )
{
   if (option==0) {
      printf("%3i%7s [%2i] <%3i|%3i,%3i> (% 8.3f,% 8.3f,% 8.3f)%8.3f %8.3f\n",
            GetId(), GetParticleName(),GetStatus(),
            GetMother(), GetFirstDaughter(), GetLastDaughter(),
            GetPx(), GetPy(), GetPz(), GetE(), GetM()
            );
      }
}


inline HEPEvent* LUJETSParticle::GetEvent()
{ 
  return (HEPEvent*) event; 
}

inline int const LUJETSParticle::GetId()
{ 
   return id;
 }

inline int const LUJETSParticle::GetMother()
{ 
   return event->GetK_(id,3);
}

inline int const LUJETSParticle::GetMother2()
{ 
   return 0;
}

inline int const LUJETSParticle::GetFirstDaughter()
{ 
   return event->GetK_(id,4);
}

inline int const LUJETSParticle::GetLastDaughter()
{ 
   return event->GetK_(id,5);
}

inline double const LUJETSParticle::GetE ()
{ 
   return event->GetP_(id,4);
}

inline double const LUJETSParticle::GetPx()
{ 
   return event->GetP_(id,1);
}

inline double const LUJETSParticle::GetPy()
{
   return event->GetP_(id,2);
}

inline double const LUJETSParticle::GetPz()
{ 
return event->GetP_(id,3);
}

inline double const LUJETSParticle::GetM()
{
   return event->GetP_(id,5);
}

inline int const LUJETSParticle::GetPDGId()
{ 
   return event->GetK_(id,2);
}

inline int const LUJETSParticle::GetStatus()
{
   return event->GetK_(id,1);
}

inline double const LUJETSParticle::GetVx()
{ 
   return event->GetV_(id,1);
}

inline double const LUJETSParticle::GetVy()
{
   return event->GetV_(id,2);
}

inline double const LUJETSParticle::GetVz()
{
   return event->GetV_(id,3);
}

inline double const LUJETSParticle::GetTau()
{ 
   return event->GetV_(id,4);
}

inline double const LUJETSParticle::GetLifetime()
{ 
   return event->GetV_(id,5);
}

inline void LUJETSParticle::SetEvent( HEPEvent  *event ) 
{
    this->event=(LUJETSEvent*)event;
//  printf("Unsupported : LUJETSParticle::SetEvent\n");
}

inline void LUJETSParticle::SetId( int id       )
{
   this->id=id;
}     

inline void LUJETSParticle::SetMother( int mother )
{ 
  event->SetK_(id,3,mother);
}

inline void LUJETSParticle::SetMother2( int mother ) 
{

}

inline void LUJETSParticle::SetFirstDaughter( int daughter )
{
   event->SetK_(id,4,daughter);
}

inline void LUJETSParticle::SetLastDaughter( int daughter )
{
    event->SetK_(id,5,daughter); 
}

inline void LUJETSParticle::SetE( double E )
{
   event->SetP_(id,4,E ); 
}

inline void LUJETSParticle::SetPx( double px )
{
   event->SetP_(id,1,px); 
}

inline void LUJETSParticle::SetPy( double py )
{ 
   event->SetP_(id,2,py); 
}

inline void LUJETSParticle::SetPz( double pz )
{
   event->SetP_(id,3,pz); 
} 

inline void LUJETSParticle::SetM( double m )
{
   event->SetP_(id,5,m ); 
}

inline void LUJETSParticle::SetPDGId( int pdg )
{
   event->SetK_(id,2,pdg); 
}

inline void LUJETSParticle::SetStatus( int st )
{ 
   event->SetK_(id,1,st); 
}

inline void LUJETSParticle::SetVx( double vx )
{
   event->SetV_(id,1,vx); 
}

inline void LUJETSParticle::SetVy( double vy )
{
   event->SetV_(id,2,vy); 
}

inline void LUJETSParticle::SetVz( double vz )
{
   event->SetV_(id,3,vz); 
}

inline void LUJETSParticle::SetTau( double tau )
{
   event->SetV_(id,4,tau); 
}

inline void LUJETSParticle::SetLifetime(double lifetime )
{
   event->SetV_(id,5,lifetime); 
}






#ifdef _USE_ROOT_
void LUJETSParticle::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}
#endif   
