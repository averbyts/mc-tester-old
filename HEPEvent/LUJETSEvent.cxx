/*
   LUJETSEvent class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 1999-01-24
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "LUJETSEvent.H"


#ifdef _USE_ROOT_
ClassImp(LUJETSEvent)
#endif


LUJETSEvent LUJETS; 

//extern "C" 
LujetsCommon lujets_ ;


LUJETSEvent::LUJETSEvent():
  particles(new LUJETSParticle[4000]),
  data( &lujets_ )
{
   for (int i=1; i<=4000;i++)
     particles[i-1].SetId(i);
}



void LUJETSEvent::SetParticle( int idx, HEPParticle *particle)
{
   printf("UNSUPPORTED method LUJETSEvent::SetParticle()\n");
}

void LUJETSEvent::AddParticle( HEPParticle *particle)
{
   printf("UNSUPPORTED method LUJETSEvent::AddParticle()\n");
}

void LUJETSEvent::Clear (int fromIdx)
{
   printf("UNSUPPORTED method LUJETSEvent::Clear()\n");
}
void LUJETSEvent::ls( char *option)
{
   printf("LUJETSEvent::ls()\n");
   HEPEvent::ls(option);

}  


//_____________________________________________________________________________
void  LUJETSEvent::InsertParticle(int at_idx,HEPParticle *p)
{
   printf("UNSUPPORTED method LUJETSEvent::AddParticle()\n");

}

//_____________________________________________________________________________
void  LUJETSEvent::AddParticle( int id, int pdgid, int status,
				int mother, int mother2,
				int firstdaughter, int lastdaughter,
				double E,double px, double py, double pz, double m,
				double vx, double vy, double vz, double tau)
{
   printf("UNSUPPORTED method LUJETSEvent::AddParticle()\n");

}




//_____________________________________________________________________________
int LUJETSEvent::GetN()
{
  // returns N value - actual number of enries in event.

  return data->n;
  
}

//_____________________________________________________________________________
void LUJETSEvent::SetN(int n)
{
  // sets N value
  data->n=n;
  
}


//_____________________________________________________________________________
int LUJETSEvent::GetK(int part,int coord)
{
   
  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::GetK(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in LUJETSEvent::GetK(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->k[coord-1][part-1];
  
}

//_____________________________________________________________________________
void LUJETSEvent::SetK(int part,int coord, int value)
{
     
  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::SetK(int part, int coord, int value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in LUJETSEvent::SetK(int part,int coord, int value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->k[coord-1][part-1]=value;
  
}








//_____________________________________________________________________________
float  LUJETSEvent::GetP(int part,int coord)
{
   
  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::GetP(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || coord>data->n ) {
    printf("ERROR in LUJETSEvent::GetP(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->p[coord-1][part-1];
  
}
//_____________________________________________________________________________
void LUJETSEvent::SetP(int part,int coord, float  value)
{
     
  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::SetP(int part, int coord, float  value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in LUJETSEvent::SetP(int part,int coord, float  value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->p[coord-1][part-1]=value;
  
}



//_____________________________________________________________________________
float  LUJETSEvent::GetV(int part,int coord)
{

  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::GetV(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in LUJETSEvent::GetV(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->v[coord-1][part-1];

}

//_____________________________________________________________________________
void LUJETSEvent::SetV(int part,int coord, float  value)
{

  if ( coord<1 || coord>5 ) {
    printf("ERROR in LUJETSEvent::SetV(int part, int coord, float  value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in LUJETSEvent::SetV(int part,int coord, float  value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->v[coord-1][part-1]=value;

}






// fast versions - no range checking...


//_____________________________________________________________________________
int LUJETSEvent::GetK_(int part,int coord)
{
  // fast vesion of LUJETSEvent::GetK - no range checking.
   return data->k[coord-1][part-1];
}
//_____________________________________________________________________________
void LUJETSEvent::SetK_(int part,int coord, int value)
{
  // fast vesion of LUJETSEvent::SetK - no range checking.
  data->k[coord-1][part-1]=value;
}


//_____________________________________________________________________________
float  LUJETSEvent::GetP_(int part,int coord)
{
  // fast vesion of LUJETSEvent::GetP - no range checking.
   return data->p[coord-1][part-1];
}
//_____________________________________________________________________________
void LUJETSEvent::SetP_(int part,int coord, float  value)
{
  // fast vesion of LUJETSEvent::SetP - no range checking.
  data->p[coord-1][part-1]=value;
}



//_____________________________________________________________________________
float  LUJETSEvent::GetV_(int part,int coord)
{
  // fast vesion of LUJETSEvent::GetV - no range checking.
   return data->v[coord-1][part-1];
}
//_____________________________________________________________________________
void LUJETSEvent::SetV_(int part,int coord, float  value)
{
  // fast vesion of LUJETSEvent::SetV - no range checking.
  data->v[coord-1][part-1]=value;
}



#ifdef _USE_ROOT_
void LUJETSEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility - dummy
}
#endif  
