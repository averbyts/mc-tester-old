/*
   HerwigParticle class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 2003-04-29
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "HerwigParticle.H"
#include "HerwigEvent.H"

#ifdef _USE_ROOT_
ClassImp(HerwigParticle)
#endif


HerwigParticle::HerwigParticle()
{
 id=1;
 event=&HerwigEVT;
}

void HerwigParticle::ls (char* option )
{
    HEPEVTParticle::ls();
}

const int HerwigParticle::IsStable()
{ 
    return (GetStatus() == 1); 
}

const int HerwigParticle::Decays()
{ 
    int s = GetStatus();
    
    return ( (s == 2) ||	// parton before hadronization
	     (s == 195) ||	// direct unstable non-hadron
	     (s == 196) ||	// direct unstable hadron (1-body clus.)
	     (s == 197) ||	// direct unstable hadron (2-body clus.)
	     (s == 198) ||	// indirect unstable hadron or lepton
	     (s == 199)		// decayed heavy flavour hadron
	      ); 
}

const int HerwigParticle::IsHistoryEntry()
{ 
    return (GetStatus() == 3); 
}
      
      


const HerwigParticle HerwigParticle::operator=(HEPParticle &p)
{

  if (this == &p)
    return *this;

  // SetId(p.GetId());

  SetPDGId(p.GetPDGId());
  SetStatus(p.GetStatus());
  SetMother(p.GetMother());
  SetMother2(p.GetMother2());
  SetFirstDaughter(p.GetFirstDaughter());
  SetLastDaughter(p.GetLastDaughter());
  SetE(p.GetE());
  SetPx(p.GetPx());
  SetPy(p.GetPy());
  SetPz(p.GetPz());
  SetM(p.GetM());
  SetVx(p.GetVx());
  SetVy(p.GetVy());
  SetVz(p.GetVz());
  SetTau(p.GetTau());

  return *this;

}


int const HerwigParticle::GetLastDaughter()
{ 
   //return event->GetJDAHEP_(2,id);

   
   int myId = GetId();
   int maxPart=event->GetNumOfParticles();
//   int d1 = event->GetJDAHEP_(1,id);
  
    int d1 = GetFirstDaughter(); 
   if (d1==0) {
//    printf("NO FIRST Daughter!\n");
    return 0;
    }
   
   int d2=d1;
   
   // find the last particle, which shows
   // at me as its mother...
   // start with the first indicated particle
   for (int i=d1; i<=maxPart; i++) {
	HEPParticle *p=event->GetParticle(i);
	if (p->GetMother() != myId) {
	    d2=i-1;
	    break;
	}
    
   }
   
   // check the last particle as well!
    if (d2==d1) {
	HEPParticle *p=event->GetParticle(maxPart);
	if (p->GetMother() == myId) d2=maxPart;
    }


// printf(" D2=%i",d2);   
   return d2;   
}


#ifdef _USE_ROOT_
void HerwigParticle::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}
#endif   
