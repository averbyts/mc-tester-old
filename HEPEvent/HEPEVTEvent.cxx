/*
   HEPEVTEvent class implementation

   AUTHOR:      Piotr Golonka 
   LAST UPDATE: 1999-04-21
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "HEPEVTEvent.H"


#ifdef _USE_ROOT_
ClassImp(HEPEVTEvent)
#endif


HEPEVTEvent HEPEVT; 

//extern "C" 
HepevtCommon hepevt_;


//_____________________________________________________________________________
HEPEVTEvent* HEPEVTEvent::CreateEvent(int size)
{
    // Creates new, HEPEVT-like event record and
    // returns its address.    
    // This record must be released after use 
    // by HEPEVTEvent::DestroyEvent()
    //
    // use with CAUTION !!!

    HepevtCommon *data = new (HepevtCommon[nmxhep]);
    HEPEVTEvent *new_event= new HEPEVTEvent(data,size);

    return new_event;

}

//_____________________________________________________________________________
void HEPEVTEvent::DestroyEvent(HEPEVTEvent *e)
{
    // Destroys event record created by CreateEvent().
    //
    // Use with CAUTION!!!
    //
    
    if ( (e!=0) && (e->data!=0) && (e!=&HEPEVT))
       delete (HepevtCommon*)(e->data);
    
    delete e;
}

//_____________________________________________________________________________
HEPEVTEvent::HEPEVTEvent(int size):
  particles(new HEPEVTParticle[size]),
  data( &hepevt_),
  _size(size)
{
   for (int i=1; i<=_size;i++)
     particles[i-1].SetId(i);
}

//_____________________________________________________________________________
HEPEVTEvent::HEPEVTEvent(void* dataptr, int size):
  particles(new HEPEVTParticle[size]),
  data(dataptr),
  _size(size)
{
  // constructs completely new, HEPEVT-like event record.

  for (int i=1; i<=_size;i++) {
    particles[i-1].SetId(i);
    particles[i-1].SetEvent(this);
  }

}

//_____________________________________________________________________________
void HEPEVTEvent::SetParticle( int idx, HEPParticle *particle)
{
  if (idx <1 || idx >_size) {
    printf("ERROR in HEPEVTEvent::SetParticle!!!\n");
    printf(" idx=%i is out of range [1..%i]\n.",idx,nmxhep);
    return;

  }

  HEPEVTParticle *p= (HEPEVTParticle*)GetParticle(idx);

  HEPParticle &rhs=*particle;
  HEPEVTParticle &lhs=*p;

  lhs=rhs; // HEPEVTParticle copy constructor !
  
}

//_____________________________________________________________________________
void HEPEVTEvent::AddParticle( HEPParticle *particle)
{
  // Appends a particle at the end of event record.


   int numpart=GetNumOfParticles();

   if (numpart >=_size) {

     printf("ERROR in HEPEVTEvent::AddParticle();\n");
     printf(" this event record is full...\n");
     return;
   }

   numpart++;

   SetNumOfParticles(numpart);

   HEPEVTParticle *next_ptr=(HEPEVTParticle*)GetParticle(numpart);

   HEPEVTParticle &next = *next_ptr;
   HEPParticle &part = *particle;

   next=part; // copy constructor of HEPEVTParticle !

}

//_____________________________________________________________________________
void  HEPEVTEvent::InsertParticle(int idx,HEPParticle *p)
{
  // insert a particle at given position in event record, shifting
  // down the rest of the list (i.e adjusting mother-daughter pointers).
  //
 
  if (idx>GetNumOfParticles()) {

    AddParticle(p);
    return;
  }
   
  // now we'll shift all particles down one step:

  // let's make a free place at the end of list:
  int numpart=GetNumOfParticles();
  SetNumOfParticles(numpart+1);


  for (int i=numpart;i>=idx;i--) {

    HEPEVTParticle *p1 = (HEPEVTParticle*) GetParticle(i);  //source
    HEPEVTParticle *p2 = (HEPEVTParticle*) GetParticle(i+1);//destination

    // HACK:we do not need to update ID...
    // p2->SetPDGId(p1->GetPDGId+1);

    p2->SetPDGId(p1->GetPDGId());
    p2->SetStatus(p1->GetStatus());



    // find where are mothers/daughters and shift if needed...

    int m1=p1->GetMother();
    if (m1>=idx) m1++;
    p2->SetMother(m1);

    int m2=p1->GetMother2();
    if (m2>=idx) m2++;
    p2->SetMother2(m2);

    int d1=p1->GetFirstDaughter();
    if (d1>=idx) d1++;
    p2->SetFirstDaughter(d1);

    int d2=p1->GetLastDaughter();
    if (d2>=idx) d2++;
    p2->SetLastDaughter(d2);

    // all the rest ...

    p2->SetE(p1->GetE());
    p2->SetPx(p1->GetPx());
    p2->SetPy(p1->GetPy());
    p2->SetPz(p1->GetPz());
    p2->SetE(p1->GetE());
    p2->SetVx(p1->GetVx());
    p2->SetVy(p1->GetVy());
    p2->SetVz(p1->GetVz());
    p2->SetTau(p1->GetTau());
  }

  // finally we'll insert the particle:
  HEPEVTParticle &lhs = *( (HEPEVTParticle*) GetParticle(idx));

  ////////////////////////////
  // HACK!!!!
  // If it happened that we want to re-insert a particle
  // that existed on the same list and was shifted down,
  // we must find its new position...

  if ( (p->GetEvent() == this ) &&
       (p->GetId() >=numpart)    ) { 
    
    HEPParticle *pp=GetParticle( p->GetId()+1);
    HEPParticle &rhs=*pp;
    lhs=rhs; // copy constructor...
    // and re-adjust mother/daughter pointers...
    
    if (lhs.GetMother() >=idx) lhs.SetMother(lhs.GetMother() -1);
    if (lhs.GetMother2() >=idx) lhs.SetMother2(lhs.GetMother2() -1);
    if (lhs.GetFirstDaughter() >=idx) lhs.SetFirstDaughter(lhs.GetFirstDaughter() -1);
    if (lhs.GetLastDaughter() >=idx) lhs.SetLastDaughter(lhs.GetLastDaughter() -1);
 
  } else { ///////////// END of HACK
  HEPParticle &rhs = *p;  
  lhs=rhs;
  }

}

//_____________________________________________________________________________
void  HEPEVTEvent::AddParticle( int id, int pdgid, int status,
				int mother, int mother2,
				int firstdaughter, int lastdaughter,
				double E,double px, double py, double pz, 
				double m,
				double vx, double vy, double vz, double tau)
{


   int numpart=GetNumOfParticles();

   if (numpart >=_size) {

     printf("ERROR in HEPEVTEvent::AddParticle();\n");
     printf(" this event record is full...\n");
     return;
   }

   numpart++;

   SetNumOfParticles(numpart);

   HEPEVTParticle *p=(HEPEVTParticle*)GetParticle(numpart);
   
   p->SetId(numpart);
   p->SetPDGId(pdgid);
   p->SetStatus(status);
   p->SetMother(mother);
   p->SetMother2(mother2);
   p->SetFirstDaughter(firstdaughter);
   p->SetLastDaughter(lastdaughter);
   p->SetE(E);
   p->SetPx(px);
   p->SetPy(py);
   p->SetPz(pz);
   p->SetM(m);
   p->SetVx(vx);
   p->SetVy(vy);
   p->SetVz(vz);
   p->SetTau(tau);

}


//_____________________________________________________________________________
void HEPEVTEvent::Clear (int fromIdx)
{

  if (fromIdx < 1 || fromIdx >= _size) {
    printf("Error in HEPEVT::Clear() fromIdx=%i is out of range[1,%i] .\n",
	   fromIdx,_size);
    return;
  }
  SetNumOfParticles(fromIdx-1);

}


//_____________________________________________________________________________
void HEPEVTEvent::ls( char *option)
{
   printf("HEPEVTEvent::ls()\n");
   HEPEvent::ls(option);
}  


//_____________________________________________________________________________
int HEPEVTEvent::GetNEVHEP()
{
  // returns NEVHEP value - event number.
  return ((HepevtCommon*)data)->nevhep;
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetNEVHEP(int n)
{
  // sets NEVHEP value - event number.

  ((HepevtCommon*)data)->nevhep=n;
  
}

//_____________________________________________________________________________
int HEPEVTEvent::GetNHEP()
{
  // returns NHEP value - actual number of enries in event.

  return ((HepevtCommon*)data)->nhep;
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetNHEP(int n)
{
  // sets NHEP value
  ((HepevtCommon*)data)->nhep=n;
  
}

//_____________________________________________________________________________
int HEPEVTEvent::GetISTHEP(int ihep)
{
  // returns (ihep)-th entry of ISTHEP array - status code for (ihep)-th
  // particle .

  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetISTHEP(int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  return ((HepevtCommon*)data)->isthep[ihep-1];
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetISTHEP(int ihep, int value)
{
  // sets ISTHEP value for (ihep)-th particle.

  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetISTHEP(int ihep, int value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return; 
  }
  
  ((HepevtCommon*)data)->isthep[ihep-1]=value;
  
}

//_____________________________________________________________________________
int HEPEVTEvent::GetIDHEP(int ihep)
{
  // returns (ihep)-th entry of IDHEP array - particle's indentification
  // code according to PDG standard.

  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetIDHEP(int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  return ((HepevtCommon*)data)->idhep[ihep-1];
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetIDHEP(int ihep, int value)
{
  // sets IDHEP value for (ihep)-th particle.
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetIDHEP(int ihep, int value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return ;
  }
  
  ((HepevtCommon*)data)->idhep[ihep-1]=value;
  
}


//_____________________________________________________________________________
int HEPEVTEvent::GetJMOHEP(int mother,int ihep)
{
  // returns mothers' pointers from JMOHEP array. 
  // Arguments:
  //            mother:  indicates which mother should be accessed:
  //                     1=first, 2=second  
  //            ihep  :  particle's number   
  //

  if ( mother<1 || mother>2 ) {
    printf("ERROR in HEPEVTEvent::GetJMOHEP(int mother, int ihep):\n");
    printf("      mother=%i is neither 1 nor 2 !\n",mother);
    return 0;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetJMOHEP(int mother,int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  return ((HepevtCommon*)data)->jmohep[ihep-1][mother-1];
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetJMOHEP(int mother,int ihep , int value)
{
  // sets mothers' pointers in JMOHEP array. 
  // Arguments:
  //            mother:  indicates which daughter should be accessed:
  //                     1=first, 2=second  
  //            ihep  :  particle's number   
  //            value :  new mother pointer for particle (ihep)
  //
  
  if ( mother<1 || mother>2 ) {
    printf("ERROR in HEPEVTEvent::SetJMOHEP(int mother, int ihep, int value):\n");
    printf("      mother=%i is neither 1 nor 2 !\n",mother);
    return ;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetJMOHEP(int mother, int ihep, int value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return ;
  }
  
  ((HepevtCommon*)data)->jmohep[ihep-1][mother-1] = value;
  
}


//_____________________________________________________________________________
int HEPEVTEvent::GetJDAHEP(int daughter,int ihep)
{
  // returns daughters' pointers from JMOHEP array. 
  // Arguments:
  //            daughter:  indicates which daughter should be accessed:
  //                       1=first, 2=last  
  //            ihep  :    particle's number   
  
  if ( daughter<1 || daughter>2 ) {
    printf("ERROR in HEPEVTEvent::GetJDAHEP(int daughter, int ihep):\n");
    printf("      daughter=%i is neither 1 nor 2 !\n",daughter);
    return 0;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetJDAHEP(int daughter,int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  
  return ((HepevtCommon*)data)->jdahep[ihep-1][daughter-1];
  
}

//_____________________________________________________________________________
void HEPEVTEvent::SetJDAHEP(int daughter,int ihep , int value)
{
  // sets daughters' pointers in JMOHEP array. 
  // Arguments:
  //            daughter:  indicates which daughter should be accessed:
  //                     1=first, 2=last  
  //            ihep  :  particle's number   
  //            value :  new daughter pointer for particle (ihep)
  //
   
  if ( daughter<1 || daughter>2 ) {
    printf("ERROR in HEPEVTEvent::SetJDAHEP(int daughter, int ihep, int value):\n");
    printf("      daughter=%i is neither 1 nor 2 !\n",daughter);
    return ;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetJDAHEP(int daughter, int ihep, int value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return ;
  }
    
  ((HepevtCommon*)data)->jdahep[ihep-1][daughter-1] = value;

}



//_____________________________________________________________________________
double HEPEVTEvent::GetPHEP(int coord,int ihep)
{
  // returns particle's momentum from PHEP array
  // Arguments:
  //           coord:  momentum 4-vector component to be accessed:
  //                   1 = x direction [GeV/c] 
  //                   2 = y direction [GeV/c] 
  //                   3 = z direction [GeV/c] 
  //                   4 = energy      [GeV]
  //                   5 = mass        [GeV/c^2]
  //           ihep:   particle's number
   
  if ( coord<1 || coord>5 ) {
    printf("ERROR in HEPEVTEvent::GetPHEP(int coord, int ihep):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetPHEP(int coord,int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  return ((HepevtCommon*)data)->phep[ihep-1][coord-1];
  
}
//_____________________________________________________________________________
void HEPEVTEvent::SetPHEP(int coord,int ihep, double value)
{
  // sets particle's momentum in PHEP array
  // Arguments:
  //           coord:  momentum 4-vector component to be accessed:
  //                   1 = x direction [GeV/c] 
  //                   2 = y direction [GeV/c] 
  //                   3 = z direction [GeV/c] 
  //                   4 = energy      [GeV]
  //                   5 = mass        [GeV/c^2]
  //           ihep:   particle's number;
  //           value:  new value for momentum component
     
  if ( coord<1 || coord>5 ) {
    printf("ERROR in HEPEVTEvent::SetPHEP(int coord, int ihep, double value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetPHEP(int coord,int ihep, double value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return ;
  }
  
  ((HepevtCommon*)data)->phep[ihep-1][coord-1]=value;
  
}



//_____________________________________________________________________________
double HEPEVTEvent::GetVHEP(int coord,int ihep)
{
  // returns particle's vertex coordinate from VHEP array
  // Arguments:
  //           coord:  vertex position 4-vector component to be accessed:
  //                   1 = x position  [mm] 
  //                   2 = y position  [mm] 
  //                   3 = z position  [mm]
  //                   4 = production time [mm/c] 
  //           ihep:   particle's number;

  if ( coord<1 || coord>4 ) {
    printf("ERROR in HEPEVTEvent::GetVHEP(int coord, int ihep):\n");
    printf("      coord=%i is out of range [1..4] !\n",coord);
    return 0;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::GetVHEP(int coord,int ihep):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return 0;
  }
  
  return ((HepevtCommon*)data)->vhep[ihep-1][coord-1];

}
//_____________________________________________________________________________
void HEPEVTEvent::SetVHEP(int coord,int ihep, double value)
{
  // sets particle's vertex coordinate in VHEP array
  // Arguments:
  //           coord:  vertex position 4-vector component to be accessed:
  //                   1 = x position  [mm] 
  //                   2 = y position  [mm] 
  //                   3 = z position  [mm]
  //                   4 = production time [mm/c] 
  //           ihep:   particle's number;
  //           value:  new value for 4-vector component

  if ( coord<1 || coord>4 ) {
    printf("ERROR in HEPEVTEvent::SetVHEP(int coord, int ihep, double value):\n");
    printf("      coord=%i is out of range [1..4] !\n",coord);
    return ;
  }
  
  if ( ihep<1 || ihep>((HepevtCommon*)data)->nhep ) {
    printf("ERROR in HEPEVTEvent::SetVHEP(int coord,int ihep, double value):\n");
    printf("      ihep=%i is out of range [1..%i] !\n",ihep,((HepevtCommon*)data)->nhep);
    return ;
  }
  
  ((HepevtCommon*)data)->vhep[ihep-1][coord-1]=value;

}






// fast versions - no range checking...







//_____________________________________________________________________________
int HEPEVTEvent::GetISTHEP_(int ihep)
{
  // fast vesion of HEPEVTEvent::GetISTHEP - no range checking.
   return ((HepevtCommon*)data)->isthep[ihep-1];
}

//_____________________________________________________________________________
void HEPEVTEvent::SetISTHEP_(int ihep, int value)
{
  // fast vesion of HEPEVTEvent::SetISTHEP - no range checking.
   ((HepevtCommon*)data)->isthep[ihep-1]=value;
}

//_____________________________________________________________________________
int HEPEVTEvent::GetIDHEP_(int ihep)
{
  // fast vesion of HEPEVTEvent::GetIDHEP - no range checking.
   return ((HepevtCommon*)data)->idhep[ihep-1];
}

//_____________________________________________________________________________
void HEPEVTEvent::SetIDHEP_(int ihep, int value)
{
  // fast vesion of HEPEVTEvent::SetIDHEP - no range checking.
   ((HepevtCommon*)data)->idhep[ihep-1]=value;
}


//_____________________________________________________________________________
int HEPEVTEvent::GetJMOHEP_(int mother,int ihep)
{
  // fast vesion of HEPEVTEvent::GetJMOHEP - no range checking.
   return ((HepevtCommon*)data)->jmohep[ihep-1][mother-1];
}

//_____________________________________________________________________________
void HEPEVTEvent::SetJMOHEP_(int mother,int ihep , int value)
{
  // fast vesion of HEPEVTEvent::SetJMOHEP - no range checking.
  ((HepevtCommon*)data)->jmohep[ihep-1][mother-1] = value;
}


//_____________________________________________________________________________
int HEPEVTEvent::GetJDAHEP_(int daughter,int ihep)
{
  // fast vesion of HEPEVTEvent::GetJDAHEP - no range checking.
   return ((HepevtCommon*)data)->jdahep[ihep-1][daughter-1];
}

//_____________________________________________________________________________
void HEPEVTEvent::SetJDAHEP_(int daughter,int ihep , int value)
{
  // fast vesion of HEPEVTEvent::SetJDAHEP - no range checking.
  ((HepevtCommon*)data)->jdahep[ihep-1][daughter-1] = value;
}


//_____________________________________________________________________________
double HEPEVTEvent::GetPHEP_(int coord,int ihep)
{
  // fast vesion of HEPEVTEvent::GetPHEP - no range checking.
   return ((HepevtCommon*)data)->phep[ihep-1][coord-1];
}
//_____________________________________________________________________________
void HEPEVTEvent::SetPHEP_(int coord,int ihep, double value)
{
  // fast vesion of HEPEVTEvent::SetPHEP - no range checking.
  ((HepevtCommon*)data)->phep[ihep-1][coord-1]=value;
}



//_____________________________________________________________________________
double HEPEVTEvent::GetVHEP_(int coord,int ihep)
{
  // fast vesion of HEPEVTEvent::GetVHEP - no range checking.
   return ((HepevtCommon*)data)->vhep[ihep-1][coord-1];
}
//_____________________________________________________________________________
void HEPEVTEvent::SetVHEP_(int coord,int ihep, double value)
{
  // fast vesion of HEPEVTEvent::SetVHEP - no range checking.
  ((HepevtCommon*)data)->vhep[ihep-1][coord-1]=value;
}



#ifdef _USE_ROOT_
void HEPEVTEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility - dummy
}
#endif  
