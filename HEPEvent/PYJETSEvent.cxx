/*
   PYJETSEvent class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 1999-01-24
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "PYJETSEvent.H"


#ifdef _USE_ROOT_
ClassImp(PYJETSEvent)
#endif


PYJETSEvent PYJETS; 

//extern "C" 
PyjetsCommon pyjets_ ;


PYJETSEvent::PYJETSEvent():
  particles(new PYJETSParticle[4000]),
  data( &pyjets_ )
{
   for (int i=1; i<=4000;i++)
     particles[i-1].SetId(i);
}



void PYJETSEvent::SetParticle( int idx, HEPParticle *particle)
{
   printf("UNSUPPORTED method PYJETSEvent::SetParticle()\n");
}

void PYJETSEvent::AddParticle( HEPParticle *particle)
{
   printf("UNSUPPORTED method PYJETSEvent::AddParticle()\n");
}

void PYJETSEvent::Clear (int fromIdx)
{
   printf("UNSUPPORTED method PYJETSEvent::Clear()\n");
}
void PYJETSEvent::ls( char *option)
{
   printf("PYJETSEvent::ls()\n");
   HEPEvent::ls(option);

}  

//_____________________________________________________________________________
void  PYJETSEvent::InsertParticle(int at_idx,HEPParticle *p)
{
   printf("UNSUPPORTED method PYJETSEvent::AddParticle()\n");

}

//_____________________________________________________________________________
void  PYJETSEvent::AddParticle( int id, int pdgid, int status,
				int mother, int mother2,
				int firstdaughter, int lastdaughter,
				double E,double px, double py, double pz, double m,
				double vx, double vy, double vz, double tau)
{
   printf("UNSUPPORTED method PYJETSEvent::AddParticle()\n");

}





//_____________________________________________________________________________
int PYJETSEvent::GetN()
{
  // returns N value - actual number of enries in event.

  return data->n;
  
}

//_____________________________________________________________________________
void PYJETSEvent::SetN(int n)
{
  // sets N value
  data->n=n;
  
}

//_____________________________________________________________________________
int PYJETSEvent::GetNPAD()
{
  // returns NPAD value 
  return data->npad;
  
}

//_____________________________________________________________________________
void PYJETSEvent::SetNPAD(int npad)
{
  // sets NPAD value
  data->npad=npad;
  
}


//_____________________________________________________________________________
int PYJETSEvent::GetK(int part,int coord)
{
   
  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::GetK(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in PYJETSEvent::GetK(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->k[coord-1][part-1];
  
}

//_____________________________________________________________________________
void PYJETSEvent::SetK(int part,int coord, int value)
{
     
  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::SetK(int part, int coord, int value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in PYJETSEvent::SetK(int part,int coord, int value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->k[coord-1][part-1]=value;
  
}








//_____________________________________________________________________________
double  PYJETSEvent::GetP(int part,int coord)
{
   
  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::GetP(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || coord>data->n ) {
    printf("ERROR in PYJETSEvent::GetP(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->p[coord-1][part-1];
  
}
//_____________________________________________________________________________
void PYJETSEvent::SetP(int part,int coord, double  value)
{
     
  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::SetP(int part, int coord, double  value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in PYJETSEvent::SetP(int part,int coord, double  value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->p[coord-1][part-1]=value;
  
}



//_____________________________________________________________________________
double  PYJETSEvent::GetV(int part,int coord)
{

  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::GetV(int part, int coord):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return 0;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in PYJETSEvent::GetV(int part,int coord):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return 0;
  }
  
  return data->v[coord-1][part-1];

}

//_____________________________________________________________________________
void PYJETSEvent::SetV(int part,int coord, double  value)
{

  if ( coord<1 || coord>5 ) {
    printf("ERROR in PYJETSEvent::SetV(int part, int coord, double  value):\n");
    printf("      coord=%i is out of range [1..5] !\n",coord);
    return ;
  }
  
  if ( part<1 || part>data->n ) {
    printf("ERROR in PYJETSEvent::SetV(int part,int coord, double  value):\n");
    printf("      part=%i is out of range [1..%i] !\n",part,data->n);
    return ;
  }
  
  data->v[coord-1][part-1]=value;

}






// fast versions - no range checking...


//_____________________________________________________________________________
int PYJETSEvent::GetK_(int part,int coord)
{
  // fast vesion of PYJETSEvent::GetK - no range checking.
   return data->k[coord-1][part-1];
}
//_____________________________________________________________________________
void PYJETSEvent::SetK_(int part,int coord, int value)
{
  // fast vesion of PYJETSEvent::SetK - no range checking.
  data->k[coord-1][part-1]=value;
}


//_____________________________________________________________________________
double  PYJETSEvent::GetP_(int part,int coord)
{
  // fast vesion of PYJETSEvent::GetP - no range checking.
   return data->p[coord-1][part-1];
}
//_____________________________________________________________________________
void PYJETSEvent::SetP_(int part,int coord, double  value)
{
  // fast vesion of PYJETSEvent::SetP - no range checking.
  data->p[coord-1][part-1]=value;
}



//_____________________________________________________________________________
double  PYJETSEvent::GetV_(int part,int coord)
{
  // fast vesion of PYJETSEvent::GetV - no range checking.
   return data->v[coord-1][part-1];
}
//_____________________________________________________________________________
void PYJETSEvent::SetV_(int part,int coord, double  value)
{
  // fast vesion of PYJETSEvent::SetV - no range checking.
  data->v[coord-1][part-1]=value;
}



#ifdef _USE_ROOT_
void PYJETSEvent::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility - dummy
}
#endif  
