/*
   MC3Vector implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 2004-03-27
*/

#include "MC3Vector.H"

#include <math.h>
#ifdef _USE_ROOT_
ClassImp(MC3Vector)
#endif

const double _PI_=3.14159265358979323846264338327;

//______________________________________________________________________
MC3Vector::MC3Vector():
#ifdef _USE_ROOT_
TObject(),
#endif
v_X0(0.0),
v_X1(0.0),
v_X2(0.0) 
{

}

//______________________________________________________________________
MC3Vector::~MC3Vector()
{

}

//______________________________________________________________________
MC3Vector::MC3Vector(double x0, double x1, double x2):
#ifdef _USE_ROOT_
TObject(),
#endif
  v_X0(x0),
  v_X1(x1),
  v_X2(x2)
{
  // Constructor   - called when object is created.
  // 
  // Sets components of 3-vector to specified values,
}

//______________________________________________________________________
MC3Vector::MC3Vector( MC3Vector const &v):
#ifdef _USE_ROOT_
TObject(),
#endif
     v_X0 ( v.v_X0 ),
     v_X1 ( v.v_X1 ),
     v_X2 ( v.v_X2 )
{
  // Copy constructor - called when a copy of an object is being created.
}


//______________________________________________________________________
MC3Vector MC3Vector::operator+( const MC3Vector v ) const
{
  // Addition operator.
  return MC3Vector( v_X0 + v.v_X0 ,
	   	    v_X1 + v.v_X1 ,
		    v_X2 + v.v_X2 );
}

//______________________________________________________________________
MC3Vector MC3Vector::operator-( const MC3Vector v ) const
{
  // Subtraction operator.
  return MC3Vector( v_X0 - v.v_X0 ,
		    v_X1 - v.v_X1 ,
		    v_X2 - v.v_X2 );
}

//______________________________________________________________________
MC3Vector MC3Vector::operator-()
{
  // Unary minus operator.
  return MC3Vector( - v_X0,
 		    - v_X1,
		    - v_X2 );
}

//______________________________________________________________________
MC3Vector MC3Vector::operator+()
{
  // Unary plus operator.
  return MC3Vector( v_X0,
		    v_X1,
		    v_X2 );
}

//______________________________________________________________________
MC3Vector& MC3Vector::operator+=( const MC3Vector v )
{
  // Self addition operator.
  v_X0 += v.v_X0;
  v_X1 += v.v_X1;
  v_X2 += v.v_X2;
  
  return *this;
}

//______________________________________________________________________
MC3Vector& MC3Vector::operator-=( const MC3Vector v )
{
  // Self subtraction operator.
 
  v_X0 -= v.v_X0;
  v_X1 -= v.v_X1;
  v_X2 -= v.v_X2;

  return *this;
}

//______________________________________________________________________
double MC3Vector::operator*( const MC3Vector &v ) const
{
  // Scalar product operator.
  return (   v_X0 * v.v_X0 
	   + v_X1 * v.v_X1 
	   + v_X2 * v.v_X2 );
}

//______________________________________________________________________
MC3Vector MC3Vector::operator%( const MC3Vector &v ) const
{
  // Vector product operator.
    return MC3Vector( v_X1*v.v_X2-v_X2*v.v_X1,
		      v_X2*v.v_X0-v_X0*v.v_X2,
		      v_X0*v.v_X1-v_X1*v.v_X0);
}




//______________________________________________________________________
MC3Vector& MC3Vector::operator=( const MC3Vector &v)
{
  // Assignment operator - called whenever one vector is assigned to another
   v_X0 = v.v_X0;
   v_X1 = v.v_X1;
   v_X2 = v.v_X2;

   return *this;
}

//______________________________________________________________________
const bool MC3Vector::operator==( const MC3Vector &v)
{
  // Equality operator.
  if( v_X0 == v.v_X0 &&
      v_X1 == v.v_X1 &&
      v_X2 == v.v_X2 )
    return true;
  else
    return false;
}



//______________________________________________________________________
void MC3Vector::ls(char *option)
{
  // ls - prints this 3-vector.
  if (option);  
  
  printf("(%.12g , %.12g , %.12g )\n", v_X0,v_X1,v_X2);
    
}

//______________________________________________________________________
double MC3Vector::Length() const
{
    return sqrt(v_X0*v_X0 + v_X1*v_X1 + v_X2*v_X2);
}

//______________________________________________________________________
double MC3Vector::Angle(const MC3Vector &w) const
{
    // Angle in radians between this vector and w

    double sp=(*this)*w;
    double cosalpha=sp/(this->Length())/w.Length();
    
    return acos(cosalpha);
}

