#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class HEPParticle-;
#pragma link C++ class HEPParticleList-;
#pragma link C++ class HEPParticleListIterator-;
#pragma link C++ class HEPEvent-;

#pragma link C++ class HEPEVTEvent-;
#pragma link C++ class HEPEVTParticle-;

#pragma link C++ class LUJETSEvent-;
#pragma link C++ class LUJETSParticle-;

#pragma link C++ class PYJETSEvent-;
#pragma link C++ class PYJETSParticle-;

#pragma link C++ class THEPEvent-;
#pragma link C++ class THEPParticle-;

#pragma link C++ class HerwigParticle-;
#pragma link C++ class HerwigEvent-;

#pragma link C++ class MC3Vector;
#pragma link C++ class MC4Vector;

#pragma link C++ global HEPEVT;
#pragma link C++ global LUJETS;
#pragma link C++ global PYJETS;
#pragma link C++ global HerwigEVT;

#endif
