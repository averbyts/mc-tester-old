// This example illustrates how MC-TESTER can be used on pythia 8 with
// events recorded in HepMC format.
// Written by Nadia Davidson, 2008, based on pythia 8.1 example main31.cc

//pythia header files
#include "Pythia.h"
#include "HepMCInterface.h"

//HepMC header files
#include "HepMC/GenEvent.h"
#include "HepMC/IO_Ascii.h"

//MC-TESTER header files
#include "Generate.h"
#include "HepMCEvent.H"

using namespace Pythia8; 

int NumberOfEvents = 10000;

int main() {
  
  //Initialize MC-TESTER
  MC_Initialize();

  HepMC::I_Pythia8 ToHepMC;

  // Initialization of pythia
  Pythia pythia;
  Event& event = pythia.event;

  //Select process and decay to B+
  pythia.readString("HardQCD:gg2bbbar = on");
  pythia.readString("HardQCD:qqbar2bbbar = on");    

  //interaction and energy
  pythia.init( 2212, 2212, 20.0);

  // Begin event loop. Generate event.
  for (int iEvent = 0; iEvent < NumberOfEvents; ++iEvent) {

    //print how many event have been generated
    if(iEvent%1000==0)
      cout<<iEvent<<" ("<<iEvent*100.0/NumberOfEvents<<"%)"<<endl;

    if (!pythia.next()) continue;

    //print the 1st event
    if (iEvent < 1) {pythia.info.list(); pythia.event.list();} 

    // Convert event record to HepMC format
    HepMC::GenEvent * HepMCEvt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( event, HepMCEvt);

    //Make new MC-TESTER HepMCEvent event and pass to the tester
    HepMCEvent temp_event(*HepMCEvt);
    MC_Analyze(&temp_event);

    //clean up
    delete HepMCEvt;
  }
 
  //Finalize MC-TESTER
  MC_Finalize();

  return 0;
}
