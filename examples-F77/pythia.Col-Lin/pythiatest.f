      PROGRAM PYTHIATEST
  
C      MAXEVENT=5000000
       MAXEVENT=10000
  
C we'll use LUJETS:
      CALL MCSETUP(0,1)
C decay of tau- PDG=15
      CALL MCSETUP(2,15) 
C  generation stage (generator number): 1
      CALL MCSETUP(1,1)
C
C Histograms set-up:
C 80 bins per histogram     
      CALL MCSETUPHBINS(120)
C default minimum bin value is 0.0D0
      CALL MCSETUPHMIN(0.0D0)
C default maximum bin value is 2.0D0
      CALL MCSETUPHMAX(2.0D0)


C initialize MC-Tester
      CALL MCTEST(-1)

C initialize generator
      CALL GENINIT

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
     
C Main Loop     

      DO I=1,MAXEVENT
      
        CALL PYEVNT()
C	CALL LUHEPC(1)
C        CALL LUEEVT(4,91.)

C make a printout of event
C	CALL MCTEST(20)

        CALL MCTEST(0)


C make a printout of event that were
C used by MC-TESTER
	CALL MCTEST(21)
C make a printout
        IF ((1000*(I/1000)).EQ.I) THEN
          R=100*I/REAL(MAXEVENT)
          WRITE(*,100),I,MAXEVENT,R
        ENDIF
      ENDDO

C finalization
      CALL MCTEST(1)



 100  FORMAT('EVENT:',I9,'/',I9,'  (',F6.2,'%)')
      END
     
     
     
     
      
      SUBROUTINE GENINIT

      COMMON/PYPARS/MSTP(200),PARP(200),MSTI(200),PARI(200)
      COMMON/LUDAT3/MDCY(500,3),MDME(2000,2),BRAT(2000),KFDP(2000,5)
      COMMON/PYSUBS/MSEL,MSUB(200),KFIN(2,-40:40),CKIN(200)

C W/Z0 pairs production
      MSEL=15
C NO ISR      
      MSTP(61)=0
C NO FSR:
      MSTP(71)=0
C NO MULTIPLE INTERACTIONS
      MSTP(81)=0
C SWITCH ON FRAGMENTATION/DECAY:
      MSTP(111)=0


C format of event record!
      MSTP(128)=0
 
C allow Z0 to decay:
      MDCY(23,1)=1
C allow W to decay:
      MDCY(24,1)=1
C Z0 decays only to tau...
      DO 20 I=156,171
        MDME(I,1)=0
 20   CONTINUE
      MDME(168,1)=1

C W decays only to tau...
      DO 30 I=172,190
        MDME(I,1)=0
 30   CONTINUE
      MDME(190,1)=1

C switch off multiple-body tau decays:
      DO 40 I=105,123
        MDME(I,1)=0
 40   CONTINUE

      CALL PYINIT("CMS","e+","e-",2000.0)
      END

