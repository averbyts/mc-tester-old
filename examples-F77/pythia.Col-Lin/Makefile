###############################################################################
#  simple 
#  example of running with single version of PYTHIA
#  file SETUP.C is not needed
#  MC-tester initialization is set and explained in pythiatest.f 
################################################################################
#
# Makefile for pythia-MCTester example.
#
# 	Author: 	Piotr Golonka
# 	Last Modified: 	18-Mar-2002  (zbw,pg
#
################################################################################


# include global default settings from make.inc file (compilers' flags, etc).
include make.inc




################################################################################
#
# MC-TESTER needs the following libraries to be linked:
#
# 1) HEPEvent - C++ interface to various F77 event records.
#    This library resides in /HEPEvent subdirectory of MC-Tester.
# 2) MCTester - main MCTester library. Its sources may be
#    found in /src subdirectory of MC-Tester
# 3) ROOT libraries. The dependencies are found by executing root-config command
#    Note that root needs to be properly installed and ROOTSYS variable set.
#    (see ROOT_INSTALL from main Tester directory for details)
#
# It is sufficient to link these libraries, to use MC-Tester with your program.
HEPEVT_LIB  = ../../lib/libHEPEvent.so
TESTER_LIB  = ../../lib/libMCTester.so
ROOT_LIBS := $(shell $(ROOTSYS)/bin/root-config --libs)
################################################################################




#
# Main executables will be stored in these:
#
PROGRAM = ./pythiatest.exe
#
# Main program source is stored in pythiatest.f file, compiled to pythiatest.o
#
PRG_OBJ = ./pythiatest.o


#
PYTHIA_LIB = ./libpythia/pythia.o ./libpythia/jetset74.o

###############################################################################
# END OF SETTINGS. Dependencies start here.
###############################################################################

# default target is to build main executable
#
all:	$(PROGRAM) 
	@echo " ***************************************************"
	@echo "  Executable file OK. Please execute:"
	@echo "    $(PROGRAM)"
	@echo " to generate events and prepare histograms,"
	@echo " then execute "
	@echo "    make move1 "
	@echo " or "
	@echo "    make move2 "
	@echo " to move result files to either"
	@echo "   /analyze/prod1     or"
	@echo "   /analyze/prod2     directory."
	@echo " ***************************************************"

#
# Linking of main program:
#
# main program needs to link:
#   * pythia/jetset library
#   * HEPEvent library
#   * Tester library
#   * ROOT libraries:

$(PROGRAM):  $(HEPEVT_LIB) $(TESTER_LIB) $(PRG_OBJ) $(PYTHIA_LIB)
	@echo "************************************"
	@echo "    Linking main program	          "
	$(LD) -o $@ $(LDFLAGS) $(PRG_OBJ) $(PYTHIA_LIB) $(TESTER_LIB) $(HEPEVT_LIB) $(ROOT_LIBS)
	@echo 
	@echo " Main program linked in $(PROGRAM)"
	@echo "************************************"	

$(HEPEVT_LIB):
	@echo "#######################################"
	@echo " You should have executed make in the toplevel"
	@echo " direcory of MC-TESTER!"
	@echo "#######################################"
	@false
$(TESTER_LIB):
	@echo "#######################################"
	@echo " You should have executed make in the toplevel"
	@echo " direcory of MC-TESTER!"
	@echo "#######################################"
	@false


#
# Cleanup targets:
#
clean:                                                     
	rm -f $(PROGRAM)  $(PRG_OBJ) *~ *.root
Clean: clean
	make -C libpythia Clean

#
# moving results to prod1 or prod2:
#
move1: mc-tester.root 
	cp -f ./mc-tester.root ../../analyze/prod1
move2: mc-tester.root 
	cp -f ./mc-tester.root ../../analyze/prod2

##############################################################################
# Other dependencies:
##############################################################################

# F77 code will be compiled using this command:
.f.o:
	$(F77) $(FFLAGS) -c -o $@ $<



# PYTHIA compilation
$(PYTHIA_LIB):
	@echo "**********************************"
	@echo " Compiling pythia library "
	@echo "**********************************"
	make -C ./libpythia


run:$(PROGRAM)
	$(PROGRAM)