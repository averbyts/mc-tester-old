\appendix
\section{Appendix: {\tt MC-TESTER} setup and input parameters (update for ref.~\cite{Golonka:2002rz})}
\label{appendix.A}

The values of the parameters used by {\tt MC-TESTER} are controlled using 
the {\tt SETUP.C} file. Some parameters may also
be controlled using {\tt FORTRAN77} interface routines 
 or C++ methods (Section \ref{appendix.C++}).
This provides runtime control over all parameters, yet allowing the user
not to have {\tt SETUP.C} at all.
One should note that {\tt SETUP.C} always has precedence over the default values set 
using {\tt F77} or C++ code: it is always looked for in the execution directory.


\subsection{Definition of parameters in the {\tt SETUP.C} file}
\label{appendix.Setup-parameters}
There are three sets of settings inside {\tt MC-TESTER} to be distinguished: 
the ones
specific to the generation phase, the ones specific to the analysis phase and the ones
that are used in both phases\footnote{Some parameters from the generation 
 phase (i.e. the description of generators) are stored inside
 an output data file. However, again for reasons of runtime control, their 
 values may be altered at the analysis time using the {\tt SETUP.C} file in 
 the analysis directory.}. We describe only new features, quoting the scope of their use.


\subsubsection{Setup::UserTreeAnalysis}
\label{option:user_event_analysis}
Type:~char* 

Scope:~generation 

Default: null

DESCRIPTION: The name of a function that allows modification of the list of stable particles before
histograms for the decay of {\tt MC-TESTER} analyzed object are 
defined/filled in.

IMPORTANT: The name that is attributed (eg. "MyUserTree") must be a
valid method name existing in a C++ script file located in the
working directory. The script must be given the same name as the method,
and ended with a ".C" suffix. For example, for the "MyUserTree" method,
the script filename would be {\tt MyUserTree.C}.
For further information on running and compiling scripts on the fly see
{\tt README.UserTreeAnalysis} in the {\tt MC-TESTER/doc/} directory.

Example of use:

\texttt{ Setup::UserTreeAnalysis = "MyUserTree";}

or for the version compiled and present in {\tt libMCTester} library:

\texttt{ Setup::UserTreeAnalysis = "UserTreeAnalysis";}

In this case, the UserTreeAnalysis.C is not needed, as the
built-in UserTreeAnalysis routine will be used.


Parameters can be passed to the function. For example

\texttt{Setup::UTA\_{}params[0]=0.05; }

\texttt{Setup::UTA\_{}params[1]=0; }

\texttt{Setup::UTA\_{}params[2]=0; }

\texttt{Setup::UTA\_{}params[3]=0;  }

\texttt{Setup::UTA\_{}params[4]=22; }

\texttt{Setup::UTA\_{}params[5]=111; }

\texttt{Setup::UTA\_{}nparams=6;}

\noindent
will pass {\tt nparams=6} parameters to the function.
For the actual meaning of the parameters if passed into
{\tt UserTreeAnalysis} as present in the library,  see section \ref{sec:Benchmarks}.

\subsubsection{Setup::mass\_power }
\label{option:massPower}

Type:~ int 

Scope:~ generation 

Default: 1

DESCRIPTION: This option changes the variable passed
for histograming, from invariant mass to a power of invariant mass, at the generation step.
It also modifies the title displayed on histograms from {\tt Mass(1)}
to an appropriate {\tt Mass(value)}, showing that the power of the mass has
been changed.

NOTE: Acceptable values: from 1 to 9. Due to properties of the Lorentz group 
when this option has value=2 it is particularly suitable for tests of spin polarization, 
see section~\ref{sec:UserTreeAnalysis}.


Example of use: 

\texttt{ Setup::mass\_power=2; //set histograms to invariant mass squared}

\subsubsection{Setup::mass\_scale\_on }
\label{option:massScaling}

Type:~ bool

Scope:~ generation 

Default: false

DESCRIPTION: This option scales invariant masses for all plots of the decay channel to invariant mass
constructed from all daughters combined. It scales the X values to the range (0,1). 

NOTE: When using this option consider setting default maximum bin value to 1.1,
for nicer graphical representation.

Example of use: 

\texttt{ Setup::mass\_scale\_on=true; //enables scaling of X axis }

\subsubsection{Setup::use\_log\_y }
\label{option:logScale}

Type:~ bool

Scope:~ analysis

Default: false

DESCRIPTION: Enables the use of logarithmic scale in all histograms
plotted by {\tt MC-TESTER}. Turning this option on will draw the histograms
 in logarithmic scale, and mark a logarithmic scale along the
right-hand-side Y axis. This option does not affect {\tt SDP} calculation or
the plot of the ratio of histograms, which remains linear. Its corresponding
linear scale is marked on the left-hand Y axis.

NOTE: This option, combined with previously presented defaults for
      UserTreeAnalysis can be particularly useful if infrared regulator
      sensitive particles, such as soft photons are present in the event records.
      See \cite{Photos_tests}.

Example of use:

\texttt{ Setup::use\_log\_y=true; //enables logarithmic scale on Y axis }

\texttt{ Setup::mass\_power=2; //set histograms to invariant mass squared}

\subsubsection{Setup::scale\_histograms }
\label{option:scaleHistogram}

Type:~ bool

Scope:~ analysis

Default: false

DESCRIPTION: This option scales histograms to the larger of the two compared
samples. This is especailly useful when the difference between samples is
significant.

NOTE: This option does not affect SDP calculation.

Example of use: 

\texttt{ Setup::scale\_histograms = true }

\subsubsection{Setup::rebin\_factor }
\label{option:rebinfactor}

Type:~ int 

Scope:~ analysis 

Default: 1

DESCRIPTION: One may want to define a large number of bins for the generation
scope of {\tt MC-TESTER}. The number of bins on the actual plots,
can be adjusted at the analysis step. The contents of consecutive 
"rebin\_factor'' bins are summed together. Calculation of the SDP parameter
is appropriately adjusted. 

NOTE: "rebin\_factor'' must be the natural divider of the number of bins 
declared during the generation scope of {\tt MC-TESTER}.


Example of use: 

\texttt{ Setup::rebin\_factor=3; //reduces no. of bins in all histograms by factor of 3}

\subsection{ {\tt C++} configuration of {\tt MC-TESTER}}
\label{appendix.C++}
The configuration of {\tt MC-TESTER} can be done directly in the main method
of the C++ generation program, without the need for a {\tt SETUP.C} file. This can be accomplished
by including the header file {\tt Setup.H} and setting parameters 
using the same syntax as described for {\tt SETUP.C} files 
(see original documentation ~\cite{Golonka:2002rz}).
Setup should be done before calling the function {\tt MC\_Initialize()}. 
Note that if parameters are set in both the generation
program and a {\tt SETUP.C} file, the values present in {\tt SETUP.C} will be given precedence.

\section{Appendix: updates from version 1.23 up to version 1.25.0}
\label{appendix.B}
\subsection{Changes introduced in version 1.24.2}

       To address the problems that are typically faced when {\tt MC-TESTER} is
installed in a new environment, or a new platform, an automated configuration
step has been implemented in version 1.24.2. The configuration files required
to set-up/compile/run {\tt MC-TESTER} may be generated through a dedicated
configuration script, which facilitates the GNU autoconf \cite{autoconf}.

To set up {\tt MC-TESTER} using the new auto-configuration facility, proceed with
the following steps:



\begin{itemize}
  \item Execute {\tt ./configure} with additional command line options: \\
        {\tt --with-HepMC=<path>} provides the path to {\tt HepMC} installation directory 
(alternatively  {\tt HEPMCLOCATION} system variable has to be set). \\
{\tt --with-root=<path>} Path to {\tt root} binaries. \\
{\tt --with-Pythia8=<path>} Path to {\tt Pythia} version  8.1 or later (this generator is used by examples only)\\
        {\tt --prefix=<path>} provides the installation path. If this option is used {\tt include/} and {\tt lib/} directories will 
be copied to this {\tt prefix <path>} when {\tt make install} will be executed. If {\tt --prefix=<path>}
is not provided, 
the default installation directory   {\tt /usr/local} will be used
  \item Execute {\tt make}; this will build {\tt MC-TESTER}.
  \item To install {\tt MC-TESTER} into the directory specified at step 1) through the 
{\tt --prefix}
parameter, execute {\tt make install}; this will copy the include files
and libraries into {\tt include/} and {\tt lib/} sub-directories.
\item It is worth to mention that  {\tt ./configure} scripts only prepare  {\tt make.inc} files.  These files are rather short  
and can be 
easily modified or created  by hand: one can also rename {\tt README-NO-CONFIG.txt} 
to {\tt make.inc} and modify it accordingly to instructions provided inside 
the file.
\end{itemize}



Further changes and bug-fixes were implemented too. However they  do not require any changes in the way the program is used. Let us nonetheless list them here:

\begin{itemize}
  \item A bug resulting in faulty functioning of the script  {\tt ANALYZE.C} was fixed. Previously, when comparing decay samples which differed by several distinct
 channels, the program was occasionally crashing.
  \item A bug resulting in faulty functioning of {\tt UserTreeAnalysis} scripts was fixed. 
The program was crashing if
 {\tt MC4Vector} was used inside the script.
  \item All offending statements resulting in compilation errors if '-ansi -pedantic'  flags were activated have been removed now.
\end{itemize}

\subsection{  LCG configuration scripts; available from version 1.24.2  }

For our project still another configuration/automake system was 
prepared
  by Dmitri Konstantinov and Oleg Zenin;
 members of the
LCG/Genser project\footnote{We have used the expertise and advice 
of that team members in organization of configuration scripts 
for our whole distribution tar-ball as well.}
 \cite{LCG,Kirsanov:2008zz}.  

For the purpose of activation of this set of autotools-based installation scripts
enter  {\tt platform} directory and execute there {\tt use-LCG-config.sh} script.
Then, installation procedure and the names of the configuration script parameters will differ from the one 
described in our paper. Instruction given in  './INSTALL' readme file created by {\tt use-LCG-config.sh} script
should be followed. One can also execute {\tt ./configure --help}, it will 
list all options available for the configuration script.

A short information on these scripts can be found in {\tt README} of main directory as well.

\subsection{ Merging {\tt MC-TESTER} output files; available from version 1.24.3}

 Interest in using the program on distributed systems, such
as the grid has been expressed on several occasions. 
This calls for new functionality: to
merge several {\tt mc-tester.root} files into a single one, 
corresponding to all event samples combined into one.


\noindent
The {\tt analyze/MERGE.C} script can be used for this purpose:
\begin{itemize}
  \item Enter the {\tt analyze/} directory.
  \item Execute {\tt root -b MERGE.C } (or {\tt root -b} and {\tt .L MERGE.C}).
  \item Type {\tt merge(<output file> , <input directory>/<first file> , [<pattern>])} (the last parameter is optional).
  \item Copy {\tt <output file>} into {\tt analyze/prod1/mc-tester.root} \\
        (or {\tt analyze/prod2/mc-tester.root}).
\end{itemize}
Example:\\
\\
{\tt root -b}\\
{\tt root [0] .L <path\_to\_script>/MERGE.C}\\
{\tt root [1] merge("out.root","samples/first.root","*.root")}\\
%{\it Merges 'samples/first.root' and all 'mc-tester\_$*$.root' files in 'samples/' directory to file 'out.root'}\\
\\

The input to the script may consist of just the {\tt <input directory>} path 
where reside the {\tt .root} files to be merged. 
 Alternatively, the name of the first {\tt MC-TESTER .root} file to be merged ( {\tt <input directory>/<first file> }) 
can be explicitly given. Generator information will be taken from this first file.
The script will search the {\tt <input directory>} to merge all files matching 
the pattern. If no pattern is provided, the default pattern is {\tt mc-tester\_$*$.root}.
The {\tt <output file>} will  feature all histograms for decay channels including user defined histograms. 
Histograms for decay channels 
of the same 
name, found in different files will be summed together. 
The histogram bin count and axis range of the first occurence will be used. 

If histograms are found with a distinct axis range or number
of bins, compared to other histograms with the same name, then the
content of these files is ignored. However, if by mistake, the
particular input file contains data for tests of another particle's
decays, then all data from this file will be taken. All decay channels
for all particles under consideration will be listed in the .pdf file
constructed by {\tt MC-TESTER} at the analysis step. Information
printed on the front page might then be inconsistent, for example,
the overall number of entries or the overall number of channels will represent decays of all particles.
 If the interest will be expressed in future, an analysis step can be adopted to handle such cases with better front page of the booklet.


If the script is used outside the {\tt MC-TESTER/analyze/} directory, 
the {\tt MCTESTERLOCATION} system variable needs to be set to the {\tt MC-TESTER} root directory.
In addition, user can create and adopt his own copy of {\tt MERGE.C}  and use it instead of the default one. 
Note that our script cannot be used with a version of {\tt MC-TESTER} older than 1.24.3. 

\subsection{ Updates introduced in version 1.24.4}
\label{appendix.changes}

Version 1.24.4 differs from 1.24.3 by comments introduced into all parts of the code. 
Thanks to their introduction also Doxygen documentation is sufficiently exhaustive.  
Bug resulting in a faulty printout of histogram names was also fixed. Finally 
modification of {\tt MERGE.C} script, necessary for proper handling of User Histograms,
was introduced.

\subsection{ Updates introduced in version 1.25.0}
\label{appendix.changes.1.25.0}

Version 1.25.0 differs from 1.24.4 by a few critical updates, number of small bugfixes. Three new options are added:

(i) timestamp is added to the generator description printout, (ii)
option of histogram scaling (see Appendix \ref{option:scaleHistogram}) (iii) and option
of saving partial results in case of a crash or an error
(see Appendix \ref{appendix.finalizeAtExit}).

Two critical updates have been introduced. First, for {\tt HepMCEvent} which sometimes caused {\tt MC-TESTER} to crash during memory deallocation.
Second, to prevent memory corruption (problem was identified
under {\tt CMS} environtment). Due to unusual behaviour of {\tt ROOT}, memory of
{\tt MC-TESTER} could have been overwritten in some cases.
Thanks to S. Lehti and I. Nugent for help in identifying and fixing the bug.

\subsection{ Saving results in case of program crash; available from version 1.25.0}
\label{appendix.finalizeAtExit}
When program crashes or an error
occurs and program aborts before {\tt MC\_Finalize() } is executed, {\tt MC-TESTER}
produces an empty {\tt ROOT} file. For user to be able to store the results
calculated so far, a special option {\tt MC\_FinalizeAtExit()} has been
introduced. This option is executed just after {\tt MC\_Initialize()}.
When this option is active, and if program ends due to an error or
crash, {\tt MC\_Finalize()} will be called.

Note, that this option is not failproof and some errors or memory corruption
will not allow {\tt MC-TESTER} to save results computed so far. This, however,
is a rare case and indicates more severe problems in the whole analysis chain.

By default, this option is switched off, because it is not supported
on all platforms and may cause compilation errors. To activate, in
{\tt make.inc} residing in {\tt MC-TESTER} directory uncomment the line:\\
\\
{\tt \#override CXXFLAGS += -DUSE\_MC\_FINALIZE\_AT\_EXIT} \\
\\
and recompile {\tt MC-TESTER}.
