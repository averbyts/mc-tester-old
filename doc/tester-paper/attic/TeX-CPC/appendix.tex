\appendix
\section{Appendix: {\tt MC-TESTER} setup and input parameters}
\label{appendix.A}

The values of the parameters used by {\tt MC-TESTER} are controlled using 
the {\tt SETUP.C} file. Some parameters may also
be controlled using {\tt FORTRAN77} interface routines 
(Section \ref{SETUP_in_F77}).
This provides a runtime control over all parameters, yet allowing the user
not to have  {\tt SETUP.C} at all.
One should note that {\tt SETUP.C} has always precedence over the values set 
using {\tt F77} code: it is always looked for in the execution directory.

Any parameter, not set using either of the methods, will have a reasonable 
default value,
which is quoted in the parameter's description below.

\subsection{\label{SETUP.C_FORMAT_AND_USE}Format and use of the {\tt SETUP.C} file}

Please refer to section \ref{SETUP.C}

\subsection{Definition of parameters in the {\tt SETUP.C} file}
\label{appendix.Setup-parameters}
There are three sets of settings inside {\tt MC-TESTER} to be distinguished: 
the ones
specific to the generation phase, the ones specific to the analysis phase and the ones
that are used in both phases\footnote{Some parameters from the generation 
> phase (i.e. the description of generators) are stored inside
> an output data file. However, again for reasons of runtime control, their 
> values may be altered at the analysis time using the {\tt SETUP.C} file in 
> the analysis directory.}. We describe them quoting the scope of their use.


\subsubsection{Setup::decay\_particle }

Type:~ int 

Scope:~ generation 

Default: 15 (\( \tau ^{-} \)) 

DESCRIPTION: the PDG code of a particle, which decay channels we want to analyze.

Example of use: 

\texttt{Setup::decay\_particle = -521; //analyze B0 decays.}


\subsubsection{Setup::EVENT }

Type:~ HEPEvent{*} 

Scope:~ generation 

Default: (HEPEVT) 

DESCRIPTION: the {\tt F77} event format used by generator. It must be supported by
the {\tt HEPEvent}
library. The possible values are: {\tt HEPEVT} (format: 4000 entries, 
double precision), {\tt LUJETS} (as in PYTHIA 5.7), {\tt PYJETS} (as in PYTHIA 6)

To change the format of the event record standards used by {\tt MC-TESTER} 
(the size of arrays
and the precision) please refer to the {\tt include/README} file.

Example of use:

\texttt{Setup::EVENT=\&LUJETS;}


\subsubsection{Setup::stage }

Type:~ int 

Scope:~ generation, analysis 

Default: - 

DESCRIPTION: Indicates whether this is a generation or analysis stage, and which
generator is being used: 0 = the analysis stage; 1 = the generation phase for the generator
1; 2 = the generation phase for the generator 2. \\This setting, %, in particular
 is responsible
for deciding which description specified by the {\tt SETUP.C} settings will be saved at
the generation phase. It is automatically set to $0$ at the analysis stage and needs to
be set by the user's program to 1 or 2 at the generation phase (it is automatically reset to 1 if 0 occurs
at the generation phase).\footnote{One of the trick in which it may be introduced to two
versions of the code, may be observed in the {\tt TAUOLA} example program, where the \char`\"{}template\char`\"{}
{\tt F77} code is preprocessed to produce two versions of the source codes, each having
a different stage set by means of the {\tt F77} interface 
(see {\tt doc/README.SETUP.F77} for details).}

Please note that at the analysis step one may freely replace the data files from the generation steps.
The description in the {\tt SETUP.C} file referring to the generator 1 will be applied to the data file
in the subdirectory {\tt analyze/prod1/} and the ones that refer to the generator 2 will apply to 
the data in {\tt analyze/prod2/}.

Example of use: (none)


\subsubsection{Setup::gen1\_desc\_1 , Setup::gen1\_desc\_2, Setup::gen1\_desc\_3}

Type:~ char{*} 

Scope:~ generation, analysis 

Default: {[}some default text with warnings{]} 

DESCRIPTION: Up to three lines containing the description of the first of used generators.
These lines will appear on the first page of the booklet produced at the analysis
step. Any proper \LaTeX{} sequences may be introduced inside, however one needs
to note the fact, that \textbackslash{} (slash) sign is interpreted as an escape
character in {\tt C/C++}, so one needs to use \textbackslash{}\textbackslash{} (double
slash) to introduce \char`\"{}\textbackslash{}\char`\"{} into output. See the example
of the use below. When specified at the generation step, this text will be saved in
the output file. If the corresponding {\tt SETUP.C} file will not alter these 
variables at the analysis phase, the text will appear on the first page of 
the booklet. However if these variables are being set in {\tt SETUP.C} 
in the analysis phase, they will have
a precedence over the ones stored in the generation files, so one may control the
text appearing in the booklet without need to re-run the generation process.

Example of use:

\texttt{Setup::gen1\_desc\_1=\char`\"{}PYTHIA version 5.7, JetSet version 7.4;
p-p at 14~TeV, \$Z\textasciicircum{}0\$ production\char`\"{}; }

\texttt{Setup::gen1\_desc\_2=\char`\"{}\$Z\textasciicircum{}0\$ decays to \$\textbackslash{}\textbackslash{}tau\textasciicircum{}-\$
exclusively. No \$\textbackslash{}\textbackslash{}pi\$ decays, No ISR/FSR.\char`\"{};}

\texttt{Setup::gen1\_desc\_3=\char`\"{}\{\textbackslash{}\textbackslash{}tt
You may replace this text in SETUP.C file.\}\char`\"{};}


\subsubsection{Setup::gen2\_desc\_1, Setup::gen2\_desc\_2, Setup::gen2\_desc\_3}

Type:~ char{*} 

Scope:~ generation, analysis 

Default: {[}some default text with warning{]} 

DESCRIPTION: The same as above, for the second generator. 

Example of use:

\texttt{Setup::gen2\_desc\_1=\char`\"{}TAUOLA LIBRARY: VERSION AA.BB\char`\"{};}

\texttt{Setup::gen2\_desc\_2=\char`\"{}.............................\char`\"{}; }

\texttt{Setup::gen2\_desc\_3=\char`\"{}\{\textbackslash{}\textbackslash{}tt
You may replace this text in SETUP.C file in analysis dir.\}\char`\"{};}

\subsubsection{Setup::result1\_path}
\label{appendix.S.result1-path}

Type:~ char{*} 

Scope:~ generation 

Default: (set automatically to \char`\"{}./mc-tester.root\char`\"{}) 

DESCRIPTION: Sets the path and a file name of the data file produced at the generation
phase. Note that the path (absolute or relative) {\bf AND} the filename needs
to be specified. Also take into account that the analysis step requires generation
output files to be placed in certain directories 
({\tt analyze/prod1}, {\tt analyze/prod2})
and to be named \char`\"{}{\tt mc-tester.root}\char`\"{}. 

Example of use:

\texttt{Setup::result1\_path = \char`\"{}/a/path/to/results/mc-tester.root\char`\"{}}


\subsubsection{Setup::result2\_path}
\label{appendix.S.result2-path}

Type:~ char{*} 

Scope:~ generation 

Default: (set automatically to \char`\"{}./mc-tester.root\char`\"{}) 

DESCRIPTION: The same as above, for the second generator. 

Example of use:

\texttt{Setup::result2\_path = \char`\"{}../prod/mc-tester.root\char`\"{}}


\subsubsection{Setup::order\_matters}

Type:~ int 

Scope:~ generation 

Default: 0 

DESCRIPTION: This switch (values 0 or 1) specifies the behavior of a routine
which searches for decay channels inside event records. By default (value: 0),
%the same particles are not recognisable, and 
the order in which decay products
are written in an event record is not important. 
However for debugging purposes it may be
useful to distinguish the order used by two generators. In that case, for example,
{[}pi- pi0 pi+{]} will be other decay channel than {[}pi+ pi- pi0{]}. At default
behavior, when the order is not taken into account, particles are sorted according
to their PDG code, and regrouped in such a way that antiparticles stay just
after corresponding particles. Example of use: 

\texttt{Setup::order\_matters = 1; }


\subsubsection{Setup::nbins}

Type:~ 2-D array: int{[}MAX\_DECAY\_MULTIPLICITY{]}{[}MAX\_DECAY\_MULTIPLICITY{]} 

Scope:~ generation 

Default: 120 

DESCRIPTION: Setup::nbins{[}n{]}{[}m{]} specifies the number of bins in
histogram of m-body invariant in the n-body decay mode. 
Look at the example below to get it clarified.
For setting default values to the whole range, use the {\tt Setup::SetHistogramDefaults()}
function described below.\\ The maximum number of decay products is equal 
to {\tt MAX\_DECAY\_MULTIPLICITY$-$1},
because arrays in {\tt C/C++} are indexed starting from 0. 
Nevertheless, we follow the convention to refer to the arrays indexes 
using the numbers of bodies in decay channel\footnote{
Elements with indexes equal to 0 are valid from C/C++ point of view,
but not used. Elements with indexes 1 are not used either: there are no
1-body decays.}.
The {\tt MAX\_DECAY\_MULTIPLICITY}
constant is defined in the {\tt src/Setup.H} source file, to be $20$ . 
In case you need 
to analyze more complex decay channels, you need to change this setting and 
recompile {\tt MC-Tester},
however we hope it is not very likely to happen. 

Example of use:

\texttt{// Assume that you need to analyze 5-body decays more thoroughly. }

\texttt{// In all 5-body decay channels, you are especially interested }

\texttt{// in analysis of histograms of mass of 3-body subsystems. }

\texttt{// Thus, you'd like to have the histograms more detailed: }

\texttt{Setup::nbins{[}5{]}{[}3{]}=256; }


\subsubsection{Setup::bin\_min}

Type:~ 2-D array: double{[}MAX\_DECAY\_MULTIPLICITY{]}{[}MAX\_DECAY\_MULTIPLICITY{]} 

Scope:~ generation 

Default: 0.0 

DESCRIPTION: Setup::bin\_min{[}n{]}{[}m{]} specifies the minimum bin value for
histogram of m-body invariant in the n-body decay mode.
Look at the example below and the description
of {\tt Setup::nbins} above for clarification. 

Example of use:

\texttt{// Assume that you need to analyze 5-body decays more thoroughly. }

\texttt{// In all 5-body decay channels, you are especially interested }

\texttt{// in analysis of histograms of mass of 3-body subsystems. }

\texttt{// You know that the mass of all subsystems will not be lower }

\texttt{// that 3.0GeV, and so should be the lower bound of histograms }

\texttt{Setup::bin\_min{[}5{]}{[}3{]}=3.0;}


\subsubsection{Setup::bin\_max}

Type:~ 2-D array: double{[}MAX\_DECAY\_MULTIPLICITY{]}{[}MAX\_DECAY\_MULTIPLICITY{]} 

Scope:~ generation

Default: 2.0 

DESCRIPTION: Setup::bin\_max{[}n{]}{[}m{]} specifies the maximum bin value 
for histogram of m-body invariant in the n-body decay mode.
 Look at the example below and the description
of {\tt Setup::nbins} above for clarification. 

Example of use:

\texttt{// Assume that you need to analyze 5-body decays more thoroughly. }

\texttt{// In all 5-body decay channels, you are especially interested }

\texttt{// in analysis of histograms of mass of 3-body subsystems. }

\texttt{// You know that the mass of all subsystems will not exceed }

\texttt{// 4.5GeV, and so should be the upper bound of histograms }

\texttt{Setup::bin\_max{[}5{]}{[}3{]}=4.5;}

\subsubsection{Setup::SetHistogramDefaults(int nbins, double min\_bin, double
max\_bin);}
\label{appendix.S.SetHistDefaults}

Type:~ function (static method of Setup class) 

Scope:~ generation 

DESCRIPTION: Sets up the default values for the number of bins, the minimum and maximum
bin for all the histograms. 

Note: {\em the dimensions and ranges of histograms processed at analysis step need to be
the same!}\footnote{The possibility to automatically re-bin the histograms will be
introduced in next release of {\tt MC-TESTER}.}

Example of use: 

\texttt{int default\_nbin=100; }

\texttt{double default\_min\_bin=0.0;}

\texttt{double default\_max\_bin=2.0;}

\texttt{Setup::SetHistogramDefaults(default\_nbin, default\_min\_bin, default\_max\_bin); }


\subsubsection{Setup::gen1\_path}

Type:~ char{*} 

Scope:~ analysis 

Default: {[}set at the generation step to the current directory{]} 

DESCRIPTION: This variable contains the path at which the first generator was
run, therefore indicates where the result file comes from. It is being initialized
at the generation step, however one may change it at the analysis step to any other
string. This path is printed at the first page of the booklet. It is also used
to search for a file named \char`\"{}version\char`\"{}. If it exists at the
path pointed by this variable, its contents are also printed in the booklet.
The version file is supposed to contain a short, one line description of 
a version of the code used by the generator, i.e. in {\tt TAUOLA} example it contains 
the strings \char`\"{}ALEPH\char`\"{}
or \char`\"{}CLEO\char`\"{} indicating two different branches of the generator 
code, which are being tested. 

Example of use:

\texttt{Setup::gen1\_path = \char`\"{}/my/new/path/of/first\_generator\char`\"{}}


\subsubsection{Setup::gen2\_path}

Type:~ char{*} 

Scope:~ analysis 

Default: {[}set at the generation step to the current directory{]}

DESCRIPTION: The same as above, for second generator. 

Example of use:

\texttt{Setup::gen2\_path = \char`\"{}/my/new/path/of/second\_generator\char`\"{}}


\subsubsection{Setup::user\_analysis}

Type:~ function pointer: double ({*})(TH1D{*},TH1D{*}) 

Scope:~ analysis 

Default: None - needs explicit specification in {\tt SETUP.C}. 

DESCRIPTION: Indicates a user-provided function to be used at the analysis step
to calculate 
%\char`\"{}shaping parameter\char`\"{}. 
{\tt SDP}.
We recommend to adopt the
choice of the function to your analysis.
Please refer to section \ref{sec:SDP-algorithms}.

Example of use:

\texttt{///// Setup analysis code (load it from file and set up)}

\texttt{gInterpreter->LoadMacro(\char`\"{}./MyAnalysis.C\char`\"{}); }

\texttt{Setup::user\_analysis=MyAnalysis; }

\texttt{printf(\char`\"{}Using Analysis code from file ./MyAnalysis.C \textbackslash{}n\char`\"{});}


\subsubsection{Setup::user\_event\_analysis}

Type:~ pointer to object: UserEventAnalysis{*} 

Scope:~ generation 

Default: None - functionality switched off

DESCRIPTION: Allows to specify the object (inheriting from
UserEventAnalysis class), which performs custom operations
on event record.

Please refer to files {\tt README.EVENT-ANALYSIS} and 
{\tt README-LC}.

Example of use:

\texttt{Setup::user\_event\_analysis=new LC\_EventAnalysis(); }


\subsubsection{Setup::SuppressDecay(int pdg);}
\label{option:SuppressDecay}

Type:~ function (static method of Setup class) 

Scope:~ generation 

DESCRIPTION: Suppresses decays of particles with PDG code given by the
parameter. The {\tt MC-TESTER} will treat these particles as if they were
stable.

Note: a maximum of 100 types of particles may be specified;

Example of use: 

\texttt{ Setup::SuppressDecay(111); // suppress pi0 decays }


\subsection{ {\tt F77} interface of {\tt MC-TESTER}.}
\label{appendix.F77}

A set of {\tt FORTRAN77} subroutines was provided to allow modification 
of some {\tt MC-TESTER} parameters. These functions are implemented in 
{\tt C++}, but can be called from the {\tt FORTRAN} program.


\subsubsection{\label{SETUP_in_F77}\texttt{SUBROUTINE MCSETUP( WHAT, VALUE) }}

\texttt{INTEGER WHAT }

\texttt{INTEGER VALUE}

{\it Description and parameters:}

\texttt{WHAT}: specifies what kind of value needs to be set 

\begin{itemize}
\item \texttt{WHAT=0} : Event record structure to be used:

\begin{itemize}
\item \texttt{VALUE=0} COMMON/HEPEVT/ in the 4k-D format
\item \texttt{VALUE=1} COMMON/LUJETS/ (i.e. Pythia 5.7)
\item \texttt{VALUE=2} COMMON/PYJETS/ (i.e. Pythia 6) 
\end{itemize}
\item \texttt{WHAT=1} : the generation stage: 

\begin{itemize}
\item \texttt{VALUE=1} or {\tt 2} for the first and the second generator, respectively.\\
Look at the {\tt TAUOLA} example -- the stage is introduced to two version of the code 
using a preprocessor.
\end{itemize}
\item \texttt{WHAT=2} : 
\begin{itemize}
\item 
\texttt{VALUE}= the PDG code of a particle, which decays 
we analyze.
\end{itemize}
\end{itemize}

\subsubsection{\texttt{SUBROUTINE MCSETUPHBINS(VALUE) }}

\texttt{INTEGER VALUE} 

{\it Description and parameters:}

Sets up the number of bins in histograms. 


\subsubsection{\texttt{SUBROUTINE MCSETUPHMIN(VALUE) }}

\texttt{DOUBLE PRECISION VALUE} 

{\it Description and parameters:}

Sets up the value of the minimum bin in histograms. 


\subsubsection{\texttt{SUBROUTINE MCSETUPHMAX(VALUE) }}

\texttt{DOUBLE PRECISION VALUE }

{\it Description and parameters:}

Sets up the value of the maximum bin in histograms. 


\subsubsection{\texttt{SUBROUTINE MCSETUPHIST(NBODY,NHIST,NBINS,MINBIN,MAXBIN) }}

\texttt{INTEGER NBODY,NHIST,NBINS }

\texttt{DOUBLE PRECISION MINBIN,MAXBIN }

{\it Description and parameters:}

Sets up the parameters for histograms of {\tt NHIST}-body subsystems in 
{\tt NBODY}-bodies decay channel. 
{\tt NBINS} is the number of bins,
{\tt MINBIN} is the minimum bin value,
{\tt MAXBIN} is the maximum bin value. 
