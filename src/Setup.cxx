/*
   Setup class implementation and static fields initialization
*/
#include "Setup.H"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "TUserAnalysis.H"

#include "TObjArray.h"

int Setup::decay_particle=15;

int Setup::stage=0;

HEPEvent* Setup::EVENT=0;

double Setup::bin_min [MAX_DECAY_MULTIPLICITY][MAX_DECAY_MULTIPLICITY]; // lower bin for i-body decay
double Setup::bin_max [MAX_DECAY_MULTIPLICITY][MAX_DECAY_MULTIPLICITY]; // upper bin for i-body decay
int    Setup::nbins   [MAX_DECAY_MULTIPLICITY][MAX_DECAY_MULTIPLICITY]; // number of bins for i-body decay                          

char* Setup::gen1_desc_1 = new char[50];
char* Setup::gen1_desc_2 = new char[50];
char* Setup::gen1_desc_3 = new char[50];

char* Setup::gen2_desc_1 = new char[50];
char* Setup::gen2_desc_2 = new char[50];
char* Setup::gen2_desc_3 = new char[50];


char*   Setup::gen1_path = new char[256];
char*   Setup::gen2_path = new char[256];


char*   Setup::result1_path=0;
char*   Setup::result2_path=0;

int	Setup::order_matters=0;

int	Setup::debug_mode=0;
long int Setup::events_cnt=0;

bool     Setup::use_log_y=false;
long int Setup::mass_power=1;

long int Setup::rebin_factor=1; 
bool     Setup::mass_scale_on=false;

bool     Setup::scale_histograms=false;

TObjArray* Setup::user_histograms=new TObjArray();



double (*Setup::user_analysis)(TH1D*,TH1D*) = 0;

UserEventAnalysis *Setup::user_event_analysis=0;

char*  Setup::UserTreeAnalysis = 0;
int    Setup::UTA_nparams      = 0;
double Setup::UTA_params[16]   = {0.0};
// used interally to pass parameters by their name...
HEPParticle* 	 Setup::UTA_particle=0;
HEPParticleList* Setup::UTA_partlist=0;


int Setup::suppress_decay[100];
int Setup::nsuppressed_decays=0;

int Setup::max_cascade_depth=0;
Setup Setup::setup;

ClassImp(Setup)


Setup::Setup() 
{

    //////////////////////////////////////////////////////////
    // Setup histograms bounds for 1- 2- ... n-body decays. //
    //////////////////////////////////////////////////////////


    // default numbers of bins
    int default_n_bins=128;

    // default maximal bin value (min==0.0)
    double default_min_bin=0.0;
    double default_max_bin=5.0;

    sprintf(Setup::gen1_desc_1,"Description of generator (1) not specified.");
    sprintf(Setup::gen1_desc_2,"Please change this text using SETUP.C file!");
    Setup::gen1_desc_3[0]='\0';

    sprintf(Setup::gen2_desc_1,"Description of generator (2) not specified..");
    sprintf(Setup::gen2_desc_2,"Please change this text using SETUP.C file!");
    Setup::gen2_desc_3[0]='\0';


    sprintf(Setup::gen1_path,"../gen1");
    sprintf(Setup::gen2_path,"../gen2");
    SetHistogramDefaults(default_n_bins,default_min_bin,default_max_bin);


}


void Setup::SetHistogramDefaults(int nbins, double min_bin, double max_bin)
{

    // fill default values...

    for (int nbody=0;nbody<MAX_DECAY_MULTIPLICITY;nbody++) {
    for (int nhist=0;nhist<MAX_DECAY_MULTIPLICITY;nhist++) {

	Setup::nbins  [nbody][nhist]=nbins;
	Setup::bin_min[nbody][nhist]=min_bin;
	Setup::bin_max[nbody][nhist]=max_bin;
    }
    }


}


char* Setup::ResolvePath(char *path, char *resolvedpath)
{
    return realpath(path,resolvedpath);
}

void Setup::SuppressDecay(int pdg)
{
    if (nsuppressed_decays>100){
	printf("NO SPACE ON DECAY SUPPRESSION LIST!\n");
	exit(-1);
    }

    suppress_decay[nsuppressed_decays]=pdg;
    nsuppressed_decays++;
}

int Setup::IsSuppressed(int pdg)
{
    if (!nsuppressed_decays) return 0;
    for (int i=0; i<nsuppressed_decays;i++) {
	if ( suppress_decay[i]==pdg) return 1;
    }
    return 0;

    
}
