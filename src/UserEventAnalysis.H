/**
 * @class UserEventAnalysis
 * @brief Advanced modifications before the analysis step
 *
 * Contains LC_EventAnalysis, HerwigEventAnalysis
 * and MadGraphEventReader class declarations
 *
 * UserEventAnalysis is a base class for classes used to modify event record
 * just before the analysis is performed. It allows to manipulate
 * the list of particles in the event record or any of its properties.
 * It can be used (as in LC_EventAnalysis) to allow MC-TESTER to anlayze
 * events that would not be possible to analyze normally
 *
 * UserEventAnalysis   - adapter for inheriting classes
 * LC_EventAnalysis    - Linear Collider Workshop-specific analysis
 * HerwigEventAnalysis - event record reorderer for HERWIG MC
 * MadGraphEventReader - reads events from MadGraph event files
 */
#ifndef _UserEventAnalysis_H_
#define _UserEventAnalysis_H_

#include "TObject.h"

#include "HEPEVTEvent.H"
#include "HEPEVTParticle.H"
#include <cstdlib>

class UserEventAnalysis    //: public TObject // 27 nov 2008
{

private:
    char        *save_event_data;
protected:
    HEPEVTEvent *save_event;
    const char  *event_name;
public:
    UserEventAnalysis();
    virtual ~UserEventAnalysis();
    
    virtual void SaveOriginalEvent(HEPEvent *e);
    virtual void RestoreOriginalEvent(HEPEvent *e);
    virtual HEPEvent* ModifyEvent(HEPEvent *e);

    virtual HEPEVTEvent* SavedEvent() {return save_event;};
    virtual const char* GetName() { return event_name;};
  //    ClassDef(UserEventAnalysis,1) // User-Level Event analysis  // 27 nov 2008
};

class LC_EventAnalysis: public UserEventAnalysis {

public:
    LC_EventAnalysis();
    virtual void SaveOriginalEvent(HEPEvent *e);
    virtual void RestoreOriginalEvent(HEPEvent *e);
    virtual HEPEvent* ModifyEvent(HEPEvent *e);
  //    ClassDef(LC_EventAnalysis,1) // Linear Collider Workshop-specific analysis

};

class HerwigEventAnalysis: public LC_EventAnalysis{

public:
    HerwigEventAnalysis();
    virtual HEPEvent* ModifyEvent(HEPEvent *e);
  //    ClassDef(HerwigEventAnalysis,1) // Event record reorderer for HERWIG MC // 27 nov 2008

};

class MadGraphEventReader:public LC_EventAnalysis{
    FILE* f;
public:

    MadGraphEventReader( const char *file_name="");
    virtual HEPEvent* ModifyEvent();
    virtual void SaveOriginalEvent(HEPEvent *e);
    virtual void RestoreOriginalEvent(HEPEvent *e);
    
    virtual int ReadNextEvent();
  //    ClassDef(MadGraphEventReader,1) // Reads events from MadGraph event files  // 27 nov 2008

};
#endif
