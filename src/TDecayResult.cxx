/*
   TDecayResult class implementation
*/
#include "TDecayResult.H"
#include <iostream>
using namespace std;

ClassImp(TDecayResult)

#include "TLatex.h"
#include "TGaxis.h"
#include "TAxis.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TMath.h"

void TDecayResult::Draw(char *option)
{
    if (!c) c=new TCanvas(GetName(),GetTitle(),600,400);
    c->SetFillColor(10);
    gStyle->SetTitleColor(10);
    c->Draw();
    c->cd();

    hdiff->Draw("HIST"); // HIST option forces OFF Error Bars
    hdiff->SetStats(0);

    c->cd();

    TPaveText *histtitle=(TPaveText*)(c->FindObject("title"));
    if (histtitle) {
	histtitle->SetFillColor(9);
    } else {
	//printf("cannot find title pad...\n");
    }

    double max1=h1->GetMaximum();
    double max2=h2->GetMaximum();
    double max= max1>max2 ? max1:max2;
    max*=1.1;

    TAxis *xaxis=hdiff->GetXaxis();
    TAxis *yaxis=hdiff->GetYaxis();
    yaxis->SetRangeUser(0,hdiff->GetMaximum()*1.2);

    double maxx=(double)xaxis->GetXmax();
    double maxy=(double)hdiff->GetMaximum();
    double miny=(double)yaxis->GetXmin();

    double scale = maxy/max;

    if(option && strcmp(option,"LOGY")==0)
    {
	double con=0.0;
	Double_t h1_entries=h1->GetEntries();
	Double_t h2_entries=h2->GetEntries();

	for(int i=0;i<=h1->GetNbinsX()+1;i++)
	{
		con = h1->GetBinContent(i);
		if(con>0.0) h1->SetBinContent(i,TMath::Log(con));
		con = h2->GetBinContent(i);
		if(con>0.0) h2->SetBinContent(i,TMath::Log(con));
	}
	scale = maxy/TMath::Log(max);
	//Reset the number of entries in the histogram to
	//what it was orginally
	h1->SetEntries(h1_entries);
	h2->SetEntries(h2_entries);
    }

    h1->Scale(scale);
    h2->Scale(scale);
    h1->SetLineColor(2);
    h1->SetStats(0);
    h1->Draw("HIST SAME");

    h2->SetLineColor(3);
    h2->SetStats(0);
    h2->Draw("HIST SAME");

    if (axis2) delete axis2;

    axis2 = new TGaxis(maxx,miny,maxx,maxy,0,max,510,"+L");

    if(option && strcmp(option,"LOGY")==0)
    {
	axis2->SetOption("+LG");
	axis2->SetWmin(1);
    }
    axis2->SetLineColor(2);
    axis2->SetTextColor(2);
    axis2->Draw();


    TPad *p=new TPad("fitparam","",0.69,0.88,0.90,0.99);
    p->SetFillColor(5);
    p->Draw();
    p->cd();
    TLatex *t1=new TLatex(0.15,0.6,"#font[21]{SDP}");
    t1->SetTextSize(0.39);

    char buf[128];
    sprintf(buf,"%7.3g",fit_parameter);
    TLatex *t3=new TLatex(0.1,0.05,buf);
    t3->SetTextSize(0.55);

    t1->Draw();
    t3->Draw();
    c->cd();

}

 void TDecayResult::Browse(TBrowser *)
 {
     Draw();
     gPad->Update();
 }

